<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSgmenuItemsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sg_sgmenu_items', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('sg_sgmenu_group_id')->unsigned();
            $table->integer('pid');
            $table->string('alias', 50);
            $table->tinyInteger('alias_type');
            $table->string('pfxclass', 255);
            $table->string('pretext', 255);
            $table->string('title', 255);
            $table->string('tag_alt', 255); 
            $table->text('description');
            $table->string('tags', 255);
            $table->boolean('activated')->default(true);
            $table->tinyInteger('type');
            $table->integer('order');
            $table->integer('article_id');
            $table->string('packets', 255);
            
            $table->string('metatitle', 255);
            $table->text('metadescription');
            $table->text('metakeywords');
            
            $table->timestamps();
            $table->index('sg_sgmenu_group_id');
            $table->index('pid');
            $table->index('alias');
            $table->index('order');
            $table->foreign('sg_sgmenu_group_id')->references('id')->on('sg_sgmenu_groups')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sg_sgmenu_items');
    }

}
