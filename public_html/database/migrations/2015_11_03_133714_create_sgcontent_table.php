<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSgcontentTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sg_sgcontents', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('sg_sgcontent_group_id')->unsigned();
            $table->integer('pid');
            $table->string('alias', 50); 
            $table->string('pfxclass', 255);
            $table->string('title', 255); 
            $table->text('pretext');
            $table->text('text');
            $table->string('tags', 255);
            $table->boolean('activated')->default(TRUE);
            $table->integer('order');  
            $table->string('image', 150);
            
            $table->string('metatitle', 255);
            $table->text('metadescription');
            $table->text('metakeywords');
            
            $table->timestamps();
            $table->index('sg_sgcontent_group_id');
            $table->index('pid');
            $table->index('alias');
            $table->index('order');
            $table->foreign('sg_sgcontent_group_id')->references('id')->on('sg_sgcontent_groups')->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sg_sgcontents');
    }

}
