<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSgmenuGroupsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sg_sgmenu_groups', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('pid')->nullable();
            $table->string('title');
            $table->text('description');
            $table->boolean('activated')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sg_sgmenu_groups');
    }

}
