<?php

use Illuminate\Database\Seeder;

class AddAdmin extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('admins')->insert([
            'name' => 'selezax',
            'email' => 'selezax@gmail.com',
            'password' => bcrypt('qwerty5'),
        ]);
    }

}
