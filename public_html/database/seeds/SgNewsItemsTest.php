<?php

use Illuminate\Database\Seeder;

class SgNewsItemsTest extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 50; $i++){
            DB::table('sg_news')->insert([
                        'sg_news_group_id'  => '1',
                        'alias'             => 'novost_' . $i ,
                        'title'             => 'Новость новость ' . $i ,
                        'text'              => 'Новость новость ' . $i . 'Новость новость ' . $i . 'Новость новость ' . $i . 'Новость новость ' . $i . 'Новость новость ' . $i . 'Новость новость ' . $i . 'Новость новость ' . $i . 'Новость новость ' . $i . 'Новость новость ' . $i . 'Новость новость ' . $i . 'Новость новость ' . $i  ,
                        'activated'         => true ,
                        'image'             => '/images/news/YEo_web-design-consultant-rates.png' ,
                        'order'             => $i ,
            ]);
        }
    }
}
