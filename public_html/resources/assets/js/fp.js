function AnimatedCss() {
    var h = $(window).height();
    this.startAnimateScroll = function (params) {
        $(window).scroll(function () {
            var obj = this;
            $.each(params, function (key, val) {
                if (($(obj).scrollTop() + h) >= $(val.divParent).offset().top ) {
                    $(val.divAnimate).css({visibility: "visible"});
                    $(val.divAnimate).addClass(val.animate + ' animated');
                }
                ;
            });
        });
    };

}
;