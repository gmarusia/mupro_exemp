<?php
return [

    'more'              => 'подробнее',
    'SiteName'          => 'Финансовая компания',
    'Error404'          => 'Ошибка 404',
    'PageNotFound'      => 'Страница не найдена',
    'LastNews'          => 'Последние новости',
    'Checkout Consult'  => 'Заказать консультацию',
];


