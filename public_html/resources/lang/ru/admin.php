<?php
return [
    
    'Administration'        => 'Администрирование',
    'Login'                 => 'Вход',
    'Logout'                => 'Выход',
    
    'E-Mail Address'        => 'E-Mail',
    'Password'              => 'Пароль',
    'Remember Me'           => 'Запомнить меня',
    'Forgot Your Password'  => 'Забыли пароль',
    
    'Settings'              => 'Настройки',
    'Dashboard'             => 'Панель управления',
    'Users'                 => 'Пользователи сайта',
    'Ausers'                => 'Администраторы сайта',
     
    'Add'                   => 'Создать',
    'Edit'                  => 'Редактировать',
    'Delete'                => 'Удалить',
    'Create'                => 'Создание',
    'Cancel'                => 'Отмена',
    'Update'                => 'Обновить',
    'Empty'                 => 'Пусто',
    'password'              => 'Пароль',
    'password_confirm'      => 'Подтвердите пароль',
    'Position Create'       => 'Позиция создана',
    'Position Update'       => 'Позиция обновлена',
    'Position Delete'       => 'Позиция удалена',
    'Yes'                   => 'Да',
    'No'                    => 'Нет',
    'AndContinue'           => ' и продолжить',
    'OrderOut'              => 'Порядок вывода',
];
