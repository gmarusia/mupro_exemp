<!-- Carousel
   ================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img class="first-slide"
                 src="/images/sl4.jpg"
                 alt="First slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Команда</h1>
                    {{-- <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous"
                        Glyphicon buttons on the left and right might not load/display properly due to web browser
                        security rules.</p>  --}}
                </div>
            </div>
        </div>
        <div class="item">
            <img class="second-slide"
                 src="/images/sl2.jpg"
                 alt="Second slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Успех</h1>
                </div>
            </div>
        </div>
        <div class="item">
            <img class="third-slide"
                 src="/images/sl3.jpg"
                 alt="Third slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Сотрудничество</h1>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">

        <span class="fa fa-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="fa fa-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->
