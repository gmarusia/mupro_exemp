@extends('layouts.frontpage')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-4">
                {{-- --------------------------------------------------------------- --}}
                {!! View::make('sgmenu::widgets.menub', [ 'group_id' => 2, 'title' => true ] )  !!}
                {{-- --------------------------------------------------------------- --}}
                <div class="clearfix">&nbsp;</div>
                <a href="#" class="btn btn-primary btn-lg" role="button" id="checkout_consult">@lang('fp.Checkout Consult')</a>
            </div>
            <div class="col-md-8">
                @include('frontpage.carousel')
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 news">
                {{-- News --}}
                {{-- --------------------------------------------------------------- --}}
                {!! View::make('sgnews::widgets.newsblog', [ 'group_alias' => 'novosti', 'amount_news' => 5 ] ) !!}
                {{-- --------------------------------------------------------------- --}}

            </div>

            <div class="col-md-8">
                {{-- --------------------------------------------------------------- --}}
                {!! View::make('sgcontent::widgets.contentblog', [ 'alias' => 'uslugi' ] )  !!}
                {{-- --------------------------------------------------------------- --}}
                <p class="clearfix">&nbsp;</p>
                <div class="col-sm-6">
                    <div class="jumbotron">
                        <p>&nbsp;</p>
                        <p class="text-center text-muted">Баннер</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="jumbotron">
                        <p>&nbsp;</p>
                        <p class="text-center text-muted">Баннер</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection

@section('footer')
    <script type="text/javascript">
        function AnimatedCss() {
            var h = $(window).height();
            this.startAnimateScroll = function (params) {
                $(window).scroll(function () {
                    var obj = this;
                    $.each(params, function (key, val) {
                        if (($(obj).scrollTop() + h) >= $(val.divParent).offset().top ) {
                            $(val.divAnimate).css({visibility: "visible"});
                            $(val.divAnimate).addClass(val.animate + ' animated');
                        }
                        ;
                    });
                });
            };

        }
        ;
        $(window).load(function () {
//            var paramsAnmCss = {    obj1: { divParent: '.about', divAnimate: '.about', animate: 'bounceInLeft' },
//                                    obj2: { divParent: '.blog-news', divAnimate: '.blog-news-item', animate: 'bounceInRight' }
//            };
//            var anmCss = new AnimatedCss();
//            anmCss.startAnimateScroll(paramsAnmCss);
        });
    </script>
@endsection