<?php /* ?>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            {{-- <a class="navbar-brand" href="{{ url('/') }}"> -  </a> --}}
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                {{-- --------------------------------------------------------------- --}}
                {!! View::make('sgmenu::widgets.menul', [ 'group_id' => 1 ] ) !!}
                {{-- --------------------------------------------------------------- --}}
            </ul>

        </div>
    </div>
</nav>
<?php */ ?>

{{-- --------------------------------------------------------------- --}}
{!! View::make('sgmenu::widgets.wsgmenu', [ 'group_id' => 1, 'type' => 'tree' ] ) !!}
{{-- --------------------------------------------------------------- --}}

