<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ua-uk" lang="ua-uk" dir="ltr" >
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Comfortaa:400,700,300&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>

        <title>@yield('metatitle', trans('fp.SiteName'))</title>

        {!! Html::style('assets/css/app.css') !!}

</head>
<body>
{{-- View::make('sgcontent::widgets.cleararticle', [ 'id' => 7, 'clear' => true ] ) --}}
    <header>
        <div class="container">
            <div class="col-sm-6 logotip">
                {{-- --------------------------------------------------------------- --}}
                {!! View::make('sgcontent::widgets.getarticle', [ 'id' => 7, 'title' => false ] ) !!}
                {{-- --------------------------------------------------------------- --}}
            </div>
            <div class="col-sm-6">
                {{-- --------------------------------------------------------------- --}}
                {!! View::make('sgcontent::widgets.getarticle', [ 'id' => 8, 'title' => false ] ) !!}
                {{-- --------------------------------------------------------------- --}}
            </div>
        </div>
    </header>

    @include('frontpage.menu')

    @if(Session::has('message'))
        <div class="alert {!! Session::get('alert-class', 'alert-info') !!} alert-dismissible fade in " role="alert" id="alert_block_top">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            {!! Session::get('message') !!}
        </div>
    @endif

    @yield('content')

    <p>&nbsp;</p>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p class="text-muted">(c) {{ date('Y') }}, @lang('fp.SiteName')</p>
                    <p class="text-muted">Сайт сделан: "Софт Генерация"</p>
                </div>
                <div class="col-sm-6">

                </div>

            </div>

        </div>
    </footer>
    <!-- JavaScripts -->
    {!! Html::script('assets/js/app.js') !!}

    @yield('footer')
    @stack('footer_scripts')

</body>
</html>
