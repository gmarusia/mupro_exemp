<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        @include('admin.inputs.text', ['fieldname' => 'name', 'fieldlabel' => trans('admins.name'), 'fieldvalue' => null ] )         
        @include('admin.inputs.email', ['fieldname' => 'email', 'fieldlabel' => trans('admins.email'), 'fieldvalue' => null ] )         
        
        @include('admin.inputs.password')
                     
    </div>
</div>
