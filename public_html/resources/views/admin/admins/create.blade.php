@extends('layouts.admin')

@section('metatitle')
    @lang('admins.Create User')
@endsection

@section('page_header')
    @lang('admins.Create User')
@endsection  

@section('tools_panel') 
    {!! Form::open(array('route' => 'adminsc.admins.store' )) !!}    
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Create'), 'route' => 'adminsc.admins.index' ])
    @endsection
@endsection


@section('content') 

    @include('admin.admins.fields')
 
    {!! Form::close() !!}
    
    @include('admin.inputs.errorlist')
    
@endsection
