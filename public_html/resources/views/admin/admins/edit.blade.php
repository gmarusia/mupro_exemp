@extends('layouts.admin')

@section('metatitle')
    @lang('admins.Edit User')
@endsection

@section('page_header')
    @lang('admins.Edit User')
@endsection  

@section('tools_panel') 
    {!! Form::model($admin, array('method' => 'PATCH', 'route' => array('adminsc.admins.update', $admin->id))) !!}   
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Update'), 'route' => 'adminsc.admins.index' ])
    @endsection
@endsection


@section('content') 

    @include('admin.admins.fields')
 
    {!! Form::close() !!}
    
    @include('admin.inputs.errorlist')
    
@endsection
