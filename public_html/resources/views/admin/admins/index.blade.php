@extends('layouts.admin')

@section('metatitle')
    @lang('admins.Manager Users')
@stop

@section('page_header') 
    @lang('admins.Manager Users')
@stop   

@section('tools_panel')
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_add', [ '_add_link' => route('adminsc.admins.create') ] )
    @endsection
@endsection

@section('content') 

    
    <table class="table table-striped table-hover table-condensed">
        <tr>
            <th>@lang('admins.id')</th>
            <th>@lang('admins.name')</th>
            <th>@lang('admins.email')</th>
            <th></th>
            <th></th> 
        </tr>
    @forelse($admins as $admin)
    <tr>
        <td>{{$admin->id}}</td>
        <td>{{$admin->name}}</td>
        <td>{{$admin->email}}</td>
        
         <td class="text-right">
            @include('admin.inputs.small.tbl_btn_edit', [ 'link' => route('adminsc.admins.edit',[ 'id' => $admin->id]) ])
        </td>
        <td>
            @include('admin.inputs.small.tbl_btn_delete', [ 'link' => route('adminsc.admins.destroy',[ 'id' => $admin->id]) ])
        </td>    
    </tr>
    @empty
    <tr>
        <td>@lang('admin.Empty')</td>
    </tr>
    @endforelse
    </table>  


@endsection
