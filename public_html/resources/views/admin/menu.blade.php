<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('dashboard') }}">@lang('admin.Administration') </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            
            @if(Auth::guard('admin')->check())
                <div class="navbar-form navbar-right">
                    <a href="{{route('admin.logout')}}" class="btn btn-danger"> 
                        <i class="fa fa-sign-out" aria-hidden="true"></i> @lang('admin.Logout') <span class="badge">{{ Auth::guard('admin')->user()->name }}</span>
                    </a>
                </div>
                <ul class="nav navbar-nav navbar-right">

                    <li><a href="{{ route('dashboard') }}">@lang('admin.Dashboard')</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">@lang('admin.Settings')<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu"> 
                            <li class="{!! Request::is('adminsc/users/*') || Request::path() == 'adminsc/users'?'active':'' !!}">
                                {!! link_to_route('adminsc.users.index', trans('admin.Users')) !!} 
                            </li>
                            <li class="{!! Request::is('adminsc/admins/*') || Request::path() == 'adminsc/admins'?'active':'' !!}">
                                {!! link_to_route('adminsc.admins.index', trans('admin.Ausers')) !!}
                            </li>
                            <?php /*  ?><li role="separator" class="divider"></li>
                            <li>{!! link_to_route('clearcache', trans('admin.Clearcache')) !!}</li> 
                            <li>{!! link_to_route('phpinfo', trans('admin.PHPInfo')) !!}</li> 
                            <li role="separator" class="divider"></li> <?php */  ?>
                        </ul>                
                    </li> 

                    @include('sgmenu::admin.menu')

                    @include('sgcontent::admin.menu')

                    @include('sgnews::admin.menu')

                    @include('sgcontact::admin.menu')


                </ul>
            
            @endif
            {{-- <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form> --}}
        </div>
    </div>
</nav>
 
