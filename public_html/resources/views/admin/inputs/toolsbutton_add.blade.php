<div class="pull-right animated zoomIn"  style="animation-delay: 0.4s;" >  
    <a href="{!! $_add_link !!}" class="btn btn-success">
        <i class="fa fa-plus" aria-hidden="true"></i> {!! $btn_label or trans('admin.Add') !!}
    </a> 
</div>
