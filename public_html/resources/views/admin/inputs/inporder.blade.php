<div class="form-group {!! $errors->has('order')?'has-error':'' !!} ">
    {!! Form::label('order',  trans('admin.OrderOut').': ') !!}
    {!! Form::input('number', 'order', isset($fieldvalue)?$fieldvalue:null , array('class'=>'form-control')) !!}
    @if($errors->has('order'))
    <ul class="text-danger small">
        @foreach ($errors->get('order') as $message)
        <li>{!! $message !!}</li>
        @endforeach
    </ul>
    @endif
</div>
