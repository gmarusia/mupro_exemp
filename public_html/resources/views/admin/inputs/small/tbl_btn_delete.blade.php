{!! Form::open(array('method' => 'DELETE', 'url' => $link, 'class' => 'form-del' )) !!}
<button type="submit" title="@lang('admin.Delete')" class= 'btn btn-danger btn-sm'>
    <i class="fa fa-times" aria-hidden="true"></i>
</button>
{!! Form::close() !!}
