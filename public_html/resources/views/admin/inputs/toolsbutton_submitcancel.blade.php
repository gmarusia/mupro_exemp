<div class="pull-right animated zoomIn"  style="animation-delay: 0.4s;" >  
    @if(isset($continue) && $continue)
        <button type="submit" class='btn btn-info ' name="submit_ok" value="submit_continue" >
            <i class="fa fa-check-square-o" aria-hidden="true"></i>
            {!! $submit !!} @lang('admin.AndContinue') 
        </button> 
    @endif
    <button type="submit" class='btn btn-info ' name="submit_ok" value="submit" >
        <i class="fa fa-check" aria-hidden="true"></i>
        {!! $submit !!}
    </button> 
    <a href="{!! route($route) !!}" class="btn btn-danger ">
        <i class="fa fa-times" aria-hidden="true"></i>
        @lang('admin.Cancel')
    </a> 
</div>
