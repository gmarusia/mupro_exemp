<div class="form-group {!! $errors->has($fieldname)?'has-error':'' !!} clearfix"> 
    {!! Form::label($fieldname, $fieldlabel .': ', [ 'class' => "col-xs-6 control-label" ]) !!}  
    <div class="col-xs-6">
        <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-primary {!! $fieldvalue?'active':'' !!} {!! $fieldname !!}-on {!! $fieldvalue?'active':'' !!}">  
                <input value="1" type="radio" name="{!! $fieldname !!}" id="{!! $fieldname !!}1" autocomplete="off" {!! $fieldvalue?'checked':'' !!} > @lang('admin.Yes')
            </label>
            <label class="btn btn-primary {!! $fieldvalue?'':'active' !!} {!! $fieldname !!}-off {!! $fieldvalue?'':'active' !!}"> 
                <input value="0" type="radio" name="{!! $fieldname !!}" id="{!! $fieldname !!}2" autocomplete="off" {!! $fieldvalue?'':'checked' !!} > @lang('admin.No')
            </label> 
        </div> 
    </div> 
    
</div>
