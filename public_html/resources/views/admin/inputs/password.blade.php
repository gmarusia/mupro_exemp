<?php 
$fieldlabel = isset($fieldlabel)?$fieldlabel:trans('admin.password')  ;
$fieldlabel_confirm = isset($fieldlabel_confirm)?$fieldlabel_confirm:trans('admin.password_confirm')  ;
        
$fieldname = isset($fieldname)?$fieldname:'password' ;
$fieldname_confirm = isset($fieldname_confirm)?$fieldname_confirm:'password_confirmation';
        
?>
<div class="form-group {!! $errors->has($fieldname)?'has-error':'' !!} ">
    {!! Form::label($fieldname, $fieldlabel .': ') !!}
    {!! Form::password($fieldname, array('class'=>'form-control')) !!}
    @if($errors->has($fieldname))
    <ul class="text-danger small">
        @foreach ($errors->get($fieldname) as $message)
        <li>{!! $message !!}</li>
        @endforeach
    </ul>
    @endif
</div>
<div class="form-group {!! $errors->has($fieldname_confirm)?'has-error':'' !!} ">
    {!! Form::label($fieldname_confirm, $fieldlabel_confirm .': ') !!}
    {!! Form::password($fieldname_confirm, array('class'=>'form-control')) !!}
    @if($errors->has($fieldname_confirm))
    <ul class="text-danger small">
        @foreach ($errors->get($fieldname_confirm) as $message)
        <li>{!! $message !!}</li>
        @endforeach
    </ul>
    @endif
</div>
