@extends('layouts.admin')

@section('tools_panel')
     
@endsection

@section('content')
<div class="container">
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default animated fadeIn"  style="animation-delay: 0.2s;">
                <div class="panel-heading"> @lang('admin.Login') </div>
                <div class="panel-body"> 
                    {!! Form::open(['url' => '/admin/login', 'class'=>"form-horizontal", 'role'=>"form", 'method'=>"POST" ] ) !!}
 
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}  animated fadeInDown"  style="animation-delay: 0.8s;" >
                        <div class="input-group col-sm-6 col-sm-offset-3 ">
                            <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i></span> 
                            {!! Form::email('email', null, [ 'class'=> "form-control", 'placeholder' => trans('admin.E-Mail Address') ] ) !!}
                        </div>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>


                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}  animated fadeInDown"  style="animation-delay: 0.6s;" >
                        <div class="input-group col-sm-6 col-sm-offset-3">
                            <span class="input-group-addon"><i class="fa fa-key fa-fw" aria-hidden="true"></i></span>
                            {!! Form::password('password', [ 'class' => "form-control", 'placeholder' => trans('admin.Password') ]) !!} 
                        </div>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>


                    <div class="form-group  animated fadeInDown"  style="animation-delay: 0.4s;" >
                        <div class="col-md-2  col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-sign-in"></i> @lang('admin.Login')
                            </button> 
                        </div>
                        <div class="col-md-3  col-sm-offset-1">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> @lang('admin.Remember Me')
                                </label>
                            </div>
                        </div> 
                    </div>
                    {!! Form::close() !!}
                    
                    @if ($errors->any())
                        <ul>
                            {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                        </ul>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
