@extends('layouts.frontpage')

@section('content')
    <div class="container">
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <h1 class="text-center">
            @lang('fp.Error404')<br/>
            @lang('fp.PageNotFound')
        </h1>

    </div>

@endsection
