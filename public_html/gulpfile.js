var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass([  'app.scss',
                'frontpage.scss',
                'frontpage_content.scss',
                'frontpage_topmenu.scss',
                'frontpage_menu2.scss',
                'frontpage_blognews.scss',
                'frontpage_footer.scss',
                'animate.css',
                'frontpage_carousel.scss'
            ], 'public/assets/css/app.css');
    mix.sass(['app.scss', 'sticky-footer-navbar.scss','admin.scss','animate.css'], 'public/assets/css/adm.css');
    
    mix.browserify(['app.js'], 'public/assets/js/app.js');
    mix.browserify(['app.js', 'admin.js'], 'public/assets/js/admin.js');
    
    // mix.version(['public/assets/css/app.css','public/assets/css/adm.css', 'public/assets/js/app.js']);
    
    mix.copy('node_modules/font-awesome/fonts', 'public/assets/fonts');
    mix.copy('node_modules/ckeditor', 'public/assets/ckeditor');

});
