<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('sgmenu::widgets.menul', '\Sg\Sgmenu\Widgets\Sgmenu');
        View::composer('sgmenu::widgets.menub', '\Sg\Sgmenu\Widgets\Sgmenu');
        View::composer('sgmenu::widgets.wsgmenu', '\Sg\Sgmenu\Widgets\Wsgmenu');

        // Если построитель реализуется при помощи класса...
        // View::composer('profile', 'App\Http\ViewComposers\ProfileComposer');

        View::composer('sgcontent::widgets.contentblog', '\Sg\Sgcontent\Widgets\Contentblog');
        View::composer('sgcontent::widgets.contentarticle', '\Sg\Sgcontent\Widgets\Contentarticle');
        View::composer('sgcontent::widgets.cleararticle', '\Sg\Sgcontent\Widgets\Contentarticle');
        View::composer('sgcontent::widgets.getarticle', '\Sg\Sgcontent\Widgets\Contentarticle');

        View::composer('sgnews::widgets.newsblog', '\Sg\Sgnews\Widgets\Newsblog');
        View::composer('sgnews::widgets.anm_newsblog', '\Sg\Sgnews\Widgets\Newsblog');

        
        
//        View::composer('sgcontent::widgets.contentarticles', '\Sg\Sgcontent\Widgets\Contentarticles');


        // Если построитель реализуется в функции-замыкании...
        /*View::composer('dashboard', function($view) {

        }); */
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
