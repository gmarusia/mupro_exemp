<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable {

    protected $fillable = [ 'name', 'email', 'password' ];
    
    protected $hidden = [ 'remember_token' ];
    
    public static $rules = [ 'name'      => 'required|max:255',
                            'email'     => 'required|email|max:255|unique:admins',
                            'password'  => 'required|confirmed|min:6', 
                        ];
    
    /**
     * Перезапись метода update
     */
    public function update(array $attributes = array(), array $options = array()) {
        if (empty($attributes['password'])) {
            unset($attributes['password']);
        }
        parent::update($attributes, $options);
    }

}
