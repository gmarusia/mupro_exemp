<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Admin;

class AdminController extends Controller {

    protected $admin;

    public function __construct(Admin $admin) {
        $this->admin = $admin;
    }

    public function index() {
        return view('admin.admins.index')
                        ->withAdmins($this->admin->all());
    }

    /**
     * Show the form for creating a new resource. 
     */
    public function create() {
        return view('admin.admins.create');
    }

    /**
     * Store a newly created resource in storage. 
     */
    public function store(Request $request) {  
        $this->validate($request, Admin::$rules);
        $_admin = $this->admin->create(['name'      => trim($request->input('name')),
                                        'email'     => trim($request->input('email')),
                                        'password'  => bcrypt(trim($request->input('password'))) 
                                    ]);
 
        return redirect(route('adminsc.admins.index'))
                        ->with(['message' => trans('admin.Position Create'), 'alert-class' => 'alert-success']);
    }
    
    /**
     * Display the specified resource. 
     */
    public function show($id) {
        return redirect(route('adminsc.admins.index'));
    }

    /**
     * Show the form for editing the specified resource. 
     */
    public function edit($id) {
        return view('admin.admins.edit')->withAdmin(Admin::findOrFail($id));
    }

    /**
     * Update the specified resource in storage. 
     */
    public function update(Request $request, $id) {
        $rules = Admin::$rules;
        $rules ['email'] = 'required|email|max:255|unique:users,email,' . $id;
        $rules ['password'] = 'confirmed|min:6';

        $this->validate($request, $rules); 
        $_admin = Admin::findOrFail($id);
        
        $request->merge(['password' => bcrypt(trim($request->input('password')))  ]);
        $_admin->update($request->all()); 
        
        return redirect(route('adminsc.admins.index'))
                        ->with(['message' => trans('admin.Position Update'), 'alert-class' => 'alert-success']);  
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id) {
        $this->admin->destroy($id);
        return redirect(route('adminsc.admins.index'))
                        ->with(['message' => trans('admin.Position Delete'), 'alert-class' => 'alert-success']);
    }

}
