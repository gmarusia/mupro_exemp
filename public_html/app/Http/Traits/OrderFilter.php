<?php

namespace App\Http\Traits;


trait OrderFilter {
    /**
     * Сортировка из сессии
     */
    public function scopeGetByOrder($query) {
        $_byOrder = \Session::get('admin.' . self::$_table . '.indexed.name', isset($this->orderDefault)?$this->orderDefault['name']:'id' );
        $_sort = \Session::get('admin.' . self::$_table . '.indexed.order', isset($this->orderDefault)?$this->orderDefault['order']:'DESC' );
        return $query->orderBy($_byOrder, $_sort);
    }

    /**
     * Установка в сессию параметров сортировки
     */
    static public function setIndexed($param) {
        $_order = [ 'ASC' => 'DESC', 'DESC' => 'ASC'];
        if (\Session::has('admin.' . self::$_table . '.indexed')) {
            $_indexed = \Session::get('admin.' . self::$_table . '.indexed');
            if ($_indexed['name'] == $param) {
                \Session::put('admin.' . self::$_table . '.indexed.order', $_order[$_indexed['order']]);
            } else {
                \Session::put('admin.' . self::$_table . '.indexed', [ 'name' => $param, 'order' => 'ASC']);
            }
        } else {
            \Session::put('admin.' . self::$_table . '.indexed', [ 'name' => $param, 'order' => 'ASC']);
        }
        return TRUE;
    }

    /**
     * Получить текущие параметры сортировки
     */
    public function getCurrentIndexed(){
        return \Session::get( 'admin.' . self::$_table . '.indexed', ['name' => isset($this->orderDefault)?$this->orderDefault['name']:'id', 'order' => isset($this->orderDefault)?$this->orderDefault['order']:'DESC'] );
    }
}