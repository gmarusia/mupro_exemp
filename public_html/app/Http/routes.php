<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return view('frontpage.index');
});


Route::auth();

Route::get('/home', 'HomeController@index');


/*
  |--------------------------------------------------------------------------
  | Admin Panel
  |--------------------------------------------------------------------------
  |
  |
 */

Route::group(['middleware' => ['web']], function () {
    Route::get ('/admin/login',  [ 'as' => 'admin.form.login', 'uses' => 'Adminauth\AuthController@showLoginForm' ]);
    Route::post('/admin/login',  [ 'as' => 'admin.post.login', 'uses' => 'Adminauth\AuthController@login']);
    Route::get ('/admin/logout', [ 'as' => 'admin.logout',     'uses' => 'Adminauth\AuthController@logout']);
});

Route::group(['prefix' => 'adminsc', 'middleware' => ['authadmin']], function() {
    Route::get('/', [ 'as' => 'dashboard', 'uses' => 'Adminauth\AuthController@index']);

    Route::resource('users', 'UserController');
    Route::resource('admins', 'AdminController');


    Route::group([  'prefix' => 'sgmenu' ], function () {
            Route::resource('groups', '\Sg\Sgmenu\Http\Controllers\GroupController');
            Route::resource('items', '\Sg\Sgmenu\Http\Controllers\ItemController');
        });

    Route::group([  'prefix' => 'sgcontent' ], function() {
            Route::resource('groups', '\Sg\Sgcontent\Http\Controllers\GroupController');
            Route::resource('contents', '\Sg\Sgcontent\Http\Controllers\ContentsController');
        });

    Route::group([  'prefix' => 'sgnews' ], function() {
            Route::resource('groups', '\Sg\Sgnews\Http\Controllers\GroupController');
            Route::resource('news',   '\Sg\Sgnews\Http\Controllers\NewsController');
        });

    Route::group([  'prefix' => 'sgcontact' ], function() {
            Route::resource('setting', '\Sg\Sgcontact\Http\Controllers\SettingController');
            Route::resource('mails',   '\Sg\Sgcontact\Http\Controllers\MailsController');
        });

});

