<?php

return [
    'cache' => [
        'blognews' => 30,
    ],
    'timezone' => 'Europe/Kiev',

    'animate_last_news' => 'fadeInRight',
    'animate_page_news' => 'fadeInUp'
];
