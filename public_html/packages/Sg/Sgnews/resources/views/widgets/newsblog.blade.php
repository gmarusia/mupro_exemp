<div class="blog-news">
    <h1>{!! $title?$title:$blog['title'] !!}</h1>
    @foreach($blog['news'] as $news)
        <div class="row blog-news-item">
            <a href="{{ route('sgnews.group.' . $blog['alias'], [ 'news' => $news['alias'] ] ) }}" >
                <div class="col-sm-4">
                    <div class="blog-news-item-imgs">
                        {!! Html::image($news['image'], $news['title'] ) !!}
                    </div>
                </div>
                <div class="col-sm-8">
                    <h2>{!! $news['title'] !!}</h2>
                    <h5 class="text-muted">{!! \Carbon\Carbon::parse($news['published_at'])->format('d.m.Y') !!}</h5>

                    {{--<div class="blog-news-item-text">{!! str_limit(strip_tags($news['text']), 250,  $end = ' ...') !!}</div>--}}
                </div>
            </a>
        </div>
    @endforeach
    <div class="row">
        <div class="col-xs-12 text-right link-all-news">
            <a href="{{ route('sgnews.ingroup.' . $blog['alias']) }}" title="@lang('sgnews::ReadAllNews') - {{ trans('fp.SiteName')}}" class=" text-warning" >
                @lang('sgnews::news.ReadAllNews') ...
            </a>
        </div>
    </div>
</div>