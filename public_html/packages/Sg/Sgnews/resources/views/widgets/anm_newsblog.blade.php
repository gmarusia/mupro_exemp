<div class="blog-news">
    <?php $i = 0.2 ?>
    <h1 class="animated {{config('sgnews.animate_last_news')}}"  style="animation-delay:{{$i}}s;" >{!! $title?$title:$blog['title'] !!}</h1>
    @foreach($blog['news'] as $news)
        <?php $i = $i + 0.2 ?>
        <div class="row blog-news-item animated {{config('sgnews.animate_last_news')}} " style="animation-delay:{{$i}}s;">
            <a href="{{ route('sgnews.group.' . $blog['alias'], [ 'news' => $news['alias'] ] ) }}" >
                <div class="col-sm-4">
                    <div class="blog-news-item-img">
                        {!! Html::image($news['image'], $news['title'] ) !!}
                    </div>
                </div>
                <div class="col-sm-8">
                    <h5 class="text-right text-info">{!! \Carbon\Carbon::parse($news['published_at'])->format('d.m.Y') !!}</h5>
                    <h2>{!! $news['title'] !!}</h2>
                    <div class="blog-news-item-text">{!! str_limit(strip_tags($news['text']), 250,  $end = ' ...') !!}</div>
                </div>
            </a>
        </div>
    @endforeach
    <div class="row">
        <div class="col-xs-12 text-right link-all-news">
            <a href="{{ route('sgnews.ingroup.' . $blog['alias']) }}" title="@lang('sgnews::ReadAllNews') - {{ trans('fp.SiteName')}}" class=" text-warning" >
                @lang('sgnews::news.ReadAllNews') ...
            </a>
        </div>
    </div>
</div>