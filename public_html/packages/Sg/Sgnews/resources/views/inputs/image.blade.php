<div class="form-group {!! $errors->has($fieldname)?'has-error':'' !!} ">
    {!! Form::label($fieldname, $fieldlabel .': ') !!}
    {!! Form::file($fieldname, array('class'=>'form-control')) !!}
    
    @if( isset($news->image) && !empty($news->image) )
        <img src='{!! $news->image !!}' class="img-thumbnail" />
    @endif
    
    @if($errors->has($fieldname))
    <ul class="text-danger small">
        @foreach ($errors->get($fieldname) as $message)
        <li>{!! $message !!}</li>
        @endforeach
    </ul>
    @endif
</div>
