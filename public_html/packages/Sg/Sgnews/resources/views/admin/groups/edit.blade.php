@extends('layouts.admin')

@section('metatitle')
    @lang('sgnews::news.Edit Group News')
@stop

@section('page_header')
    @lang('sgnews::news.Edit Group News')
@stop  



@section('tools_panel')
    {!! Form::model($group, array('method' => 'PATCH', 'route' => array('adminsc.sgnews.groups.update', $group->id))) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Update'), 'route' => 'adminsc.sgnews.groups.index', 'continue' => true ])
    @endsection
@endsection


@section('content') 

    @include('sgnews::admin.groups.fields')

    {!! Form::close() !!}

    @include('admin.inputs.errorlist')

@stop
