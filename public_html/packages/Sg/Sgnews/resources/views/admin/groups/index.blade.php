@extends('layouts.admin')

@section('metatitle')
    @lang('sgnews::news.NewsManager')
@stop

@section('page_header')
    @lang('sgnews::news.NewsManager')
@stop


@section('tools_panel')
    @parent
@section('in_tools_panel')
    @include('sgnews::admin.news.groupitem', [ 'clsContents' =>'', 'clsGroups' => 'active' ])
    @include('admin.inputs.toolsbutton_add', [ '_add_link' => route('adminsc.sgnews.groups.create'), 'btn_label' => trans('sgnews::news.Add new Groups') ] )
@endsection
@endsection


@section('content') 


<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>@lang('sgnews::news.Id')</th>
            <th>@lang('sgnews::news.NameGroup')</th>
            <th>@lang('sgnews::news.Alias')</th>
            <th>@lang('sgnews::news.Decription')</th>
            <th>@lang('sgnews::news.Active')</th>
            <th> </th>
            <th> </th>
        </tr>
    </thead>
    
    @forelse($groups as $group)
        <tr>
            <td>{!! $group->id !!}</td>
            <td>{!! $group->title !!}</td> 
            <td>{!! $group->alias !!}</td> 
            <td>{!! $group->description !!}</td> 
            <td>
                @include('admin.inputs.small.tbl_icon_onoff', [ 'marker' => $group->activated ])
            </td> 
            <td class="text-right">
                @include('admin.inputs.small.tbl_btn_edit', [ 'link' => route('adminsc.sgnews.groups.edit',[ 'id' => $group->id]) ])
            </td>
            <td>
                @include('admin.inputs.small.tbl_btn_delete', [ 'link' => route('adminsc.sgnews.groups.destroy',[ 'id' => $group->id]) ])
            </td>    
        </tr>
    @empty
        <tr>
            <td colspan="6" >@lang('sgnews::news.Not groups')</td>
        </tr>
    @endforelse    
     
    <tbody>    
</table>

    

@stop
