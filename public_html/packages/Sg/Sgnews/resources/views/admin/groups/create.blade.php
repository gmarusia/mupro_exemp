@extends('layouts.admin')

@section('metatitle')
    @lang('sgnews::news.Create Group News')
@stop

@section('page_header')
    @lang('sgnews::news.Create Group News')
@stop  


@section('tools_panel')
    {!! Form::open(array('route' => 'adminsc.sgnews.groups.store', 'files'=>true )) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Create'), 'route' => 'adminsc.sgnews.groups.index' ])
    @endsection
@endsection


@section('content') 

    @include('sgnews::admin.groups.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')

@stop
