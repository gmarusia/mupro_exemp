<div class="container">
    <div class="row">
        <div class="col-md-5">

            @include('admin.inputs.onoff', ['fieldname' => 'activated', 'fieldlabel' => trans('sgmenu::sgmenu.Activated'), 'fieldvalue' => isset($group)?$group->activated:1 ])

            @include('sgnews::inputs.text', ['fieldname' => 'alias', 'fieldlabel' => trans('sgnews::news.Alias'), 'fieldvalue' => null ])

            @include('sgnews::inputs.text', ['fieldname' => 'pfxclass', 'fieldlabel' => trans('sgnews::news.PrefixClass'), 'fieldvalue' => null ])

        </div>
        <div class="col-md-6 col-md-offset-1">

            @include('sgnews::inputs.text', ['fieldname' => 'title', 'fieldlabel' => trans('sgnews::news.GroupName'), 'fieldvalue' => null ])

            @include('sgnews::inputs.textarea', ['fieldname' => 'text', 'fieldlabel' => trans('sgnews::news.GroupText'), 'fieldvalue' => null, 'class' => '' ])

            <hr/>

            @include('sgnews::inputs.textarea', ['fieldname' => 'description', 'fieldlabel' => trans('sgnews::news.GroupDescription'), 'fieldvalue' => null, 'class' => '' ])

        </div>
    </div>
</div>