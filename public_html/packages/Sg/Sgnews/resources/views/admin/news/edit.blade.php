@extends('layouts.admin')

@section('metatitle')
    @lang('sgnews::news.Edit News')
@stop

@section('page_header')
    @lang('sgnews::news.Edit News')
@stop  


@section('tools_panel')
    {!! Form::model($news, array('method' => 'PATCH', 'route' => array('adminsc.sgnews.news.update', $news->id), 'files'=>true)) !!}

    @parent
    @section('in_tools_panel')
        <span class="badge">Id: {{$news->id}} - {{$news->title}}</span>
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Update'), 'route' => 'adminsc.sgnews.news.index', 'continue' => true ])
    @endsection
@endsection



@section('content') 

@include('sgnews::admin.news.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')
 
@stop

@section('footer')
@stop    
