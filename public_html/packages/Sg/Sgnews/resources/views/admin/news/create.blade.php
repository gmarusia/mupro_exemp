@extends('layouts.admin')

@section('metatitle')
    @lang('sgnews::news.Create News')
@stop

@section('page_header')
    @lang('sgnews::news.Create News')
@stop  


@section('tools_panel')
    {!! Form::open(array('route' => 'adminsc.sgnews.news.store', 'files'=>true )) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Create'), 'route' => 'adminsc.sgnews.news.index' ])
    @endsection
@endsection


@section('content') 

    @include('sgnews::admin.news.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')
@stop

@section('footer') 

@stop                
