@extends('layouts.admin')

@section('metatitle')
    @lang('sgnews::news.NewsManager')
@stop

@section('page_header')
    @lang('sgnews::news.NewsManager')
@stop


@section('tools_panel')
    @parent
    @section('in_tools_panel')
        @include('sgnews::admin.news.groupitem', [ 'clsContents' =>'active', 'clsGroups' => '' ])
        @include('admin.inputs.toolsbutton_add', [ '_add_link' => route('adminsc.sgnews.news.create'), 'btn_label' => trans('sgnews::news.Add News') ] )
    @endsection
@endsection


@section('content') 

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>@lang('sgnews::news.Id')</th>
            <th>@lang('sgnews::news.Title')</th>
            <th>@lang('sgnews::news.Alias')</th>
            <th>@lang('sgnews::news.OrderOut')</th>
            <th>@lang('sgnews::news.Active')</th>
            <th> </th>
            <th> </th>
        </tr>
    </thead>
    
    @forelse($news as $item)
        <tr>
            <td>{!! $item['id'] !!}</td>
            <td>{!! $item['title'] !!}</td>  
            <td>{!! $item['alias'] !!}</td>  
            <td class="text-center">{!! $item['order'] !!}</td>  
            <td>
                @include('admin.inputs.small.tbl_icon_onoff', [ 'marker' => $item->activated ])
            </td> 
            <td class="text-right">
                @include('admin.inputs.small.tbl_btn_edit', [ 'link' => route('adminsc.sgnews.news.edit',[ 'id' => $item->id]) ])
            </td>
            <td>
                @include('admin.inputs.small.tbl_btn_delete', [ 'link' => route('adminsc.sgnews.news.destroy',[ 'id' => $item->id]) ])
            </td>    
        </tr>
    @empty
        <tr>
            <td colspan="6" >@lang('sgnews::news.Not news')</td>
        </tr>
    @endforelse
    <tbody>    
</table>

{!! $news->render() !!}
    

@stop
