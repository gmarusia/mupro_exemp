{!! Form::open([ 'method' => 'GET', 'route' => ['adminsc.sgnews.contents.index'], 'class' => "form-horizontal" ]) !!}

<div class="form-group">
    {!! Form::label( 'group_id', trans('sgnews::news.NameGroup') . ': ', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-3">
        {!! Form::select('group_id', $groups, \Session::has('sgnews_contents.sg_sgnews_group_id')?\Session::get('sgnews_contents.sg_sgnews_group_id'):null, array('class'=>'form-control')) !!}
    </div>
    {!! Form::submit(trans('sgnews::news.SetFilter'), array('class' => 'btn btn-warning btn-sm')) !!}
</div>

{!! Form::close() !!}
