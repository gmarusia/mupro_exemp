<div class="btn-group" role="group" aria-label="..."> 
  <a href="{!! route('adminsc.sgnews.groups.index') !!}" class="btn btn-primary btn-sm {!! $clsGroups or '' !!} ">@lang('sgnews::news.NewsGroups')</a>
  <a href="{!! route('adminsc.sgnews.news.index') !!}" class="btn btn-primary btn-sm {!! $clsContents or '' !!}">@lang('sgnews::news.NewsItems')</a>
</div>
