<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
       aria-expanded="false">@lang('sgnews::news.NewsManager')<span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
        <li class="{!! Request::is('adminsc/sgnews/groups/*') || Request::path() == 'adminsc/sgnews/groups'?'active':'' !!}">
            {!! link_to_route('adminsc.sgnews.groups.index', trans('sgnews::news.NewsGroups')) !!}
        </li>
        <li class="{!! Request::is('adminsc/sgnews/contents/*') || Request::path() == 'adminsc/sgnews/contents'?'active':'' !!}">
            {!! link_to_route('adminsc.sgnews.news.index', trans('sgnews::news.NewsItems')) !!}
        </li>
    </ul>
</li>
