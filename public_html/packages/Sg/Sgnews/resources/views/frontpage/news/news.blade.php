@extends('layouts.frontpage')

@section('metatitle')
    {!! $news->title !!} - {!! $news->groups->title !!}
@stop


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">

                <div class="onenews {!! $news->pfxclass !!}">
                    <div class="row">
                        <div class="col-sm-10 onenews-title">
                            <h2>{!! $news->title !!}</h2>
                        </div>
                        <div class="col-sm-2 onenews-date text-right text-info">
                            <p>{!! $news->getPublishDate() !!}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 news-text">
                            {!! Html::image($news->image,$news->title, [ 'class' => 'onenews-img' ]) !!}
                            {!! $news->text !!}
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-4 blog-lastnews">
                {{-- --------------------------------------------------------------- --}}
                {!! View::make('sgnews::widgets.anm_newsblog', [ 'group_alias' => 'novosti', 'amount_news' => 10, 'title' => trans('fp.LastNews') ] ) !!}
                {{-- --------------------------------------------------------------- --}}
            </div>

        </div>




    </div>

@stop
