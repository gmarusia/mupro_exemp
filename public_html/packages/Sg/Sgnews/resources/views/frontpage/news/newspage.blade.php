@extends('layouts.frontpage')

@section('content')
    <div class="container">

        <div class="blog-news">
            <h1> {{$group->title}} </h1>
            <?php $i = 0.2 ?>
            @foreach($news as $item)
                <?php $i = $i + 0.2 ?>
                <div class="row blog-news-item animated {{config('sgnews.animate_page_news')}} " style="animation-delay:{{$i}}s;">
                    <a href="{{  route('sgnews.group.' . $group->alias, [ 'news' => $item->alias  ] ) }}" >
                        <div class="col-sm-2">
                            <div class="blog-news-item-img">
                                {!! Html::image($item->image , $item->title  ) !!}
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <h5 class="text-right text-info">{!! \Carbon\Carbon::parse($item->published_at )->format('d.m.Y') !!}</h5>
                            <h2>{!! $item->title  !!}</h2>
                            <div class="blog-news-item-text">{!! str_limit(strip_tags($item->text ), 450,  $end = ' ...') !!}</div>
                        </div>
                    </a>
                </div>
            @endforeach

        </div>
        {!! $news->render() !!}

    </div>
@endsection
