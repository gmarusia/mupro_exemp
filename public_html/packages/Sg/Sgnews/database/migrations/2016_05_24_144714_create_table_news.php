<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sg_news', function(Blueprint $table) {
            $table->increments('id');
            $table->boolean('activated')->default(TRUE);
            $table->integer('order')->unsigned();
            $table->integer('sg_news_group_id')->unsigned();
            $table->string('alias', 50);
            $table->string('pfxclass', 255);
            $table->string('title', 255);
            $table->text('pretext');
            $table->text('text');
            $table->string('tags', 255);
            $table->string('image', 255);

            $table->string('metatitle', 255);
            $table->text('metadescription');
            $table->text('metakeywords');

            $table->timestamp('published_at');
            $table->timestamps();

            $table->index('sg_news_group_id');
            $table->index('published_at');
            $table->index('created_at');
            $table->index('activated');
            $table->index('alias');
            $table->index('order');
            $table->foreign('sg_news_group_id')->references('id')->on('sg_news_group')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sg_news');
    }
}
