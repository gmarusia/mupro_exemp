<?php

namespace Sg\Sgnews\Models;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model {

    protected $table = 'sg_news_group';
    protected $fillable = [ 'activated',
                            'alias',
                            'title',
                            'description',
                            'pid',
                            'text',
                            'pfxclass'
                        ];

    protected $guarded = ['id', 'created_at', 'updated_at']; 
    
    public static $rules = [    'alias'     => 'required|max:255|unique:sg_news_group',
                                'activated' => 'required|boolean'
                            ];
    
    public function setAliasAttribute($value) {
        $this->attributes['alias'] = str_slug( $value ) ;
    }

    public function news() {
        return $this->hasMany('Sg\Sgnews\Models\News', 'sg_news_group_id', 'id');
    }
     
    public function subcategories() {
        return $this->hasMany('Sg\Sgnews\Models\Groups', 'pid');
    }
    
    /**
     * Фильтровать по "включено"
     */
    public function scopeOfActivated($query) {
        return $query->where('activated', '=', TRUE );
    }  

    /**
     * Массив для input select
     */
    public static function getGroupsListForSelect(){
        return self::OfActivated()->lists('title','id'); 
    }  
    
    /**
     * Получить категорию по алиасу со статьями
     */
    public static function getGroupByAliasWithArticles($alias){
        return self::OfActivated()->whereAlias($alias)->with('news')->first();
    }
}
