<?php

namespace Sg\Sgnews\Widgets;

use Illuminate\Contracts\View\View;
use Sg\Sgnews\Lib\GetNews;

class Newsblog {

    public function compose(View $view) {
        $viewdata = $view->getData();  
        $view->with([ 'blog' => GetNews::getGroupByAliasWithArticlesLimit($viewdata['group_alias'], $viewdata['amount_news']),
                      'title' => isset($viewdata['title'])?$viewdata['title']:false,
                    ]);
    }

}
