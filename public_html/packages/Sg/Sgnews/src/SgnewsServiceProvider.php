<?php

namespace Sg\Sgnews;

use Illuminate\Support\ServiceProvider;

class SgnewsServiceProvider extends ServiceProvider {

    public function register() {
        $this->mergeConfigFrom(__DIR__ . '/../config/sgnews.php', 'sgnews');
        
        $this->app->bind('sgnews', function () {
            return new Sgnews;
        });
//        $this->app->bind('itemsevent', function () {
//            return new Lib\Itemsevent;
//        });
        
    }

    public function boot() {

        $this->publishes([__DIR__ . '/../config/sgnews.php' => config_path('sgnews.php')], 'config');
        $this->publishes([__DIR__ . '/../database/migrations' => $this->app->databasePath() . '/migrations'], 'migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'sgnews');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang/', 'sgnews');
        
//        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
//        $loader->alias('Itemsevent', 'Sg\Sgmenu\Facades\Itemsevent');

        require __DIR__ . '/Http/routes.php';
    }
     
}
