<?php
/**
 * Created by PhpStorm.
 * User: selezax
 * Date: 25.05.16
 * Time: 16:01
 */

namespace Sg\Sgnews\Lib;

use Sg\Sgnews\Models\Groups;
use Sg\Sgnews\Models\News;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;


class GetNews
{
    public static function getGroupByAliasWithArticlesLimit($group_alias, $amount_news)
    {
        return Cache::remember('sg_blognewsAlias_' . $group_alias . $amount_news , config('sgnews.cache.blognews'), function() use($group_alias, $amount_news) {
            return
                Groups::ofActivated()
                    ->whereAlias($group_alias)
                    ->with(['news' => function ($q) use ($amount_news) {
                        $q->where('published_at', '<', Carbon::now(config('sgnews.timezone')))
                            ->orderBy('published_at', 'DESC')
                            ->ofActivated()
                            ->take($amount_news);
                    }])
                    ->first()
                    ->toArray()
                ;
        });
    }
}