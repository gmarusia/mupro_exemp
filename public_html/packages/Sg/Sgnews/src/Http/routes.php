<?php
Route::group(['namespace' => 'Sg\Sgnews\Http\Controllers'], function () {
    foreach(Sg\Sgnews\Models\Groups::ofActivated()->select('alias')->get() as $newsitem){
        Route::get('/' . $newsitem->alias . '/{news}.html', [ 'as' => 'sgnews.group.' . $newsitem->alias, 'uses' => 'Fp_newsController@shownews' ]);
        Route::get('/' . $newsitem->alias , [ 'as' => 'sgnews.ingroup.' . $newsitem->alias, 'uses' => 'Fp_newsController@showAllnews' ]);
    }
});