<?php

namespace Sg\Sgnews\Http\Controllers;

use App\Http\Controllers\Controller; 
use Sg\Sgnews\Models\News;
use Sg\Sgnews\Models\Groups;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request; 

class Fp_newsController extends Controller {

    protected $news;

    public function __construct(News $news) {
        $this->news = $news;
    }

    /**
     * Показать новось по алиасу
     */
    public function shownews($news) { 
        if($_news = News::getNewsByAlias($news)){
            return view('sgnews::frontpage.news.news')->withNews($_news);
        }
        abort('404');
    }

    /**
     * Показать все новости из категории
     */
    public function showAllnews(){
        $group = Groups::ofActivated()
            ->whereAlias(\Request::path())
            ->first();

        if($_news = $group
            ->news()
            ->orderBy('order', 'DESK')
            ->ofActivated()
            ->paginate(20)
        ){
            return view('sgnews::frontpage.news.newspage')
                ->withGroup($group)
                ->withNews($_news);
        }
        abort('404');
    }
}
