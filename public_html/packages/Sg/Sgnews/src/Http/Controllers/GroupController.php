<?php

namespace Sg\Sgnews\Http\Controllers;

use App\Http\Controllers\Controller;
use Sg\Sgnews\Models\Groups;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class GroupController extends Controller {

    protected $groups;

    public function __construct(Groups $groups) {
        $this->groups = $groups;
    }

    public function index() {
        return view('sgnews::admin.groups.index')
                ->withGroups(Groups::paginate(25));
    }

    /**
     * Форма создания новой группы меню
     */
    public function create() {
        return view('sgnews::admin.groups.create');
    }

    /**
     * Запись из формы создания новой группы меню
     */
    public function store(Request $request) {

        $this->validate($request, Groups::$rules);

        $this->groups->create($request->all());

        return redirect(route('adminsc.sgnews.groups.index'))
                        ->with(['message' => trans('admin.Position Create'), 'alert-class' => 'alert-success']);
    }

    /**
     * Display the specified resource. 
     */
    public function show($id) {
        return redirect(route('adminsc.sgnews.groups.index'));
    }

    /**
     * Форма редактирования группы меню
     */
    public function edit($id) {
        return view('sgnews::admin.groups.edit', [ 'group' => Groups::find($id)]);
    }

    /**
     * Запись формы редактирования меню
     */
    public function update(Request $request, $id) {
        $rules = Groups::$rules;
        $rules['alias'] = 'required|max:255|unique:sg_news_group,alias,' . $id;
        $this->validate($request, $rules);

        $groups = $this->groups->find($id);
        $groups->update($request->all());

        if($request->input('submit_ok') == 'submit_continue'){
            return redirect(route('adminsc.sgnews.groups.edit', [ 'id' => $id ]))
                ->with(['message' => trans('admin.Position Update'), 'alert-class' => 'alert-success']);
        }
        
        return redirect(route('adminsc.sgnews.groups.index'))
                        ->with(['message' => trans('admin.Position Update'), 'alert-class' => 'alert-success']);
    }

    /**
     * Удаление группы меню
     */
    public function destroy($id) {
        $this->groups->destroy($id);
        return redirect(route('adminsc.sgnews.groups.index'))
                        ->with(['message' => trans('admin.Position Delete'), 'alert-class' => 'alert-success']);
    }

}
