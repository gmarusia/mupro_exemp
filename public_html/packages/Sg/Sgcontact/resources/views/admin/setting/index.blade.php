@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontact::sgcnt.NewsManager')
@stop

@section('page_header')
    @lang('sgcontact::sgcnt.NewsManager')
@stop


@section('tools_panel')
    @parent
@section('in_tools_panel')
    @include('sgcontact::admin.news.groupitem', [ 'clsContents' =>'', 'clsGroups' => 'active' ])
    @include('admin.inputs.toolsbutton_add', [ '_add_link' => route('adminsc.sgcontact.groups.create'), 'btn_label' => trans('sgcontact::sgcnt.Add new Groups') ] )
@endsection
@endsection


@section('content') 


<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>@lang('sgcontact::sgcnt.Id')</th>
            <th>@lang('sgcontact::sgcnt.NameGroup')</th>
            <th>@lang('sgcontact::sgcnt.Alias')</th>
            <th>@lang('sgcontact::sgcnt.Decription')</th>
            <th>@lang('sgcontact::sgcnt.Active')</th>
            <th> </th>
            <th> </th>
        </tr>
    </thead>
    
    @forelse($groups as $group)
        <tr>
            <td>{!! $group->id !!}</td>
            <td>{!! $group->title !!}</td> 
            <td>{!! $group->alias !!}</td> 
            <td>{!! $group->description !!}</td> 
            <td>
                @include('admin.inputs.small.tbl_icon_onoff', [ 'marker' => $group->activated ])
            </td> 
            <td class="text-right">
                @include('admin.inputs.small.tbl_btn_edit', [ 'link' => route('adminsc.sgcontact.groups.edit',[ 'id' => $group->id]) ])
            </td>
            <td>
                @include('admin.inputs.small.tbl_btn_delete', [ 'link' => route('adminsc.sgcontact.groups.destroy',[ 'id' => $group->id]) ])
            </td>    
        </tr>
    @empty
        <tr>
            <td colspan="6" >@lang('sgcontact::sgcnt.Not groups')</td>
        </tr>
    @endforelse    
     
    <tbody>    
</table>

    

@stop
