@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontact::sgcnt.Setting')
@stop

@section('page_header')
    @lang('sgcontact::sgcnt.Setting')
@stop  



@section('tools_panel')
    {!! Form::model($setting, array('method' => 'PATCH', 'route' => array('adminsc.sgcontact.setting.update', $setting->id))) !!}
    @parent
    @section('in_tools_panel')
        <button type="submit" class='btn btn-info pull-right' name="submit_ok" value="submit" >
            <i class="fa fa-check" aria-hidden="true"></i>
            @lang('sgcontact::sgcnt.Save')
        </button>
    @endsection
@endsection


@section('content') 

    @include('sgcontact::admin.setting.fields')

    {!! Form::close() !!}

    @include('admin.inputs.errorlist')

@stop
