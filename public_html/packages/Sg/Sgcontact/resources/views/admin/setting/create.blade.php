@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontact::sgcnt.Create Group News')
@stop

@section('page_header')
    @lang('sgcontact::sgcnt.Create Group News')
@stop  


@section('tools_panel')
    {!! Form::open(array('route' => 'adminsc.sgcontact.groups.store', 'files'=>true )) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Create'), 'route' => 'adminsc.sgcontact.groups.index' ])
    @endsection
@endsection


@section('content') 

    @include('sgcontact::admin.groups.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')

@stop
