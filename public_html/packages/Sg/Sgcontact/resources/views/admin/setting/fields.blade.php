<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

            @include('sgcontact::inputs.text', ['fieldname' => 'reception', 'fieldlabel' => trans('sgcontact::sgcnt.EmailReception'), 'fieldvalue' => null ])
            @include('sgcontact::inputs.text', ['fieldname' => 'subject', 'fieldlabel' => trans('sgcontact::sgcnt.Subject'), 'fieldvalue' => null ])

            @include('sgcontact::inputs.textarea', ['fieldname' => 'text', 'fieldlabel' => trans('sgcontact::sgcnt.Text'), 'fieldvalue' => null, 'class' => 'cktextarea' ])

            <hr/>

            @include('sgcontact::inputs.textarea', ['fieldname' => 'maps', 'fieldlabel' => trans('sgcontact::sgcnt.MapsCode'), 'fieldvalue' => null, 'class' => '' ])

        </div>
    </div>
</div>

@push('footer_scripts')
{!! Html::script('assets/ckeditor/ckeditor.js') !!}
<script>
    $(function () {
        $('.cktextarea').each(function (index) {
            CKEDITOR.replace($(this).attr('name'),{
                allowedContent: true,
                filebrowserBrowseUrl : '/elfinder/ckeditor' });
        });

    });
</script>
@endpush