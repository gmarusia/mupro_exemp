<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
       aria-expanded="false">@lang('sgcontact::sgcnt.ContactManager')<span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
        <li class="{!! Request::is('adminsc/sgcontact/setting/*') || Request::path() == 'adminsc/sgcontact/setting'?'active':'' !!}">
            {!! link_to_route('adminsc.sgcontact.setting.edit', trans('sgcontact::sgcnt.Setting'), ['id'=>1]) !!}
        </li>
        <li class="{!! Request::is('adminsc/sgcontact/contents/*') || Request::path() == 'adminsc/sgcontact/contents'?'active':'' !!}">
            {!! link_to_route('adminsc.sgcontact.mails.index', trans('sgcontact::sgcnt.Mails')) !!}
        </li>
    </ul>
</li>
