@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontact::sgcnt.NewsManager')
@stop

@section('page_header')
    @lang('sgcontact::sgcnt.NewsManager')
@stop


@section('tools_panel')
    @parent
    @section('in_tools_panel')
        @include('sgcontact::admin.news.groupitem', [ 'clsContents' =>'active', 'clsGroups' => '' ])
        @include('admin.inputs.toolsbutton_add', [ '_add_link' => route('adminsc.sgcontact.news.create'), 'btn_label' => trans('sgcontact::sgcnt.Add News') ] )
    @endsection
@endsection


@section('content') 

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>@lang('sgcontact::sgcnt.Id')</th>
            <th>@lang('sgcontact::sgcnt.Title')</th>
            <th>@lang('sgcontact::sgcnt.Alias')</th>
            <th>@lang('sgcontact::sgcnt.OrderOut')</th>
            <th>@lang('sgcontact::sgcnt.Active')</th>
            <th> </th>
            <th> </th>
        </tr>
    </thead>
    
    @forelse($news as $item)
        <tr>
            <td>{!! $item['id'] !!}</td>
            <td>{!! $item['title'] !!}</td>  
            <td>{!! $item['alias'] !!}</td>  
            <td class="text-center">{!! $item['order'] !!}</td>  
            <td>
                @include('admin.inputs.small.tbl_icon_onoff', [ 'marker' => $item->activated ])
            </td> 
            <td class="text-right">
                @include('admin.inputs.small.tbl_btn_edit', [ 'link' => route('adminsc.sgcontact.news.edit',[ 'id' => $item->id]) ])
            </td>
            <td>
                @include('admin.inputs.small.tbl_btn_delete', [ 'link' => route('adminsc.sgcontact.news.destroy',[ 'id' => $item->id]) ])
            </td>    
        </tr>
    @empty
        <tr>
            <td colspan="6" >@lang('sgcontact::sgcnt.Not news')</td>
        </tr>
    @endforelse
    <tbody>    
</table>

{!! $news->render() !!}
    

@stop
