@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontact::sgcnt.Create News')
@stop

@section('page_header')
    @lang('sgcontact::sgcnt.Create News')
@stop  


@section('tools_panel')
    {!! Form::open(array('route' => 'adminsc.sgcontact.news.store', 'files'=>true )) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Create'), 'route' => 'adminsc.sgcontact.news.index' ])
    @endsection
@endsection


@section('content') 

    @include('sgcontact::admin.news.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')
@stop

@section('footer') 

@stop                
