<div class="container">
    <div class="row">
        <div class="col-md-4 text-primary ">
            @include('admin.inputs.onoff', ['fieldname' => 'activated', 'fieldlabel' => trans('sgmenu::sgmenu.Activated'), 'fieldvalue' => isset($news)?$news->activated:1 ])

            @include('admin.inputs.inporder', [ 'fieldvalue' => isset($orderout)?$orderout:$news->order])


            @include('sgcontact::inputs.select', ['fieldname' => 'sg_news_group_id', 'fieldlabel' => trans('sgcontact::sgcnt.NameGroup'), 'fieldvalue' => null, 'arrayDate' => $groups ])
            @include('sgcontact::inputs.text', ['fieldname' => 'pfxclass', 'fieldlabel' => trans('sgcontact::sgcnt.PrefixClass'), 'fieldvalue' => null ])
        </div>
        
        <div class="col-md-4 ">       
            @include('sgcontact::inputs.text', ['fieldname' => 'title', 'fieldlabel' => trans('sgcontact::sgcnt.Title'), 'fieldvalue' => null ])
            @include('sgcontact::inputs.text', ['fieldname' => 'alias', 'fieldlabel' => trans('sgcontact::sgcnt.Alias'), 'fieldvalue' => null ])
            
            @include('sgcontact::inputs.image', ['fieldname' => 'image', 'fieldlabel' => trans('sgcontact::sgcnt.ImageContent'), 'fieldvalue' => null ])
             
        </div> 
        
        <div class="col-md-4 text-warning ">      
            @include('sgcontact::inputs.text', ['fieldname' => 'metatitle', 'fieldlabel' => trans('sgcontact::sgcnt.Metatitle'), 'fieldvalue' => null, 'class' => 'metainfo' ])
            @include('sgcontact::inputs.textarea', ['fieldname' => 'metadescription', 'fieldlabel' => trans('sgcontact::sgcnt.Metadescription'), 'fieldvalue' => null, 'class' => 'metainfo'  ])
            @include('sgcontact::inputs.text', ['fieldname' => 'metakeywords', 'fieldlabel' => trans('sgcontact::sgcnt.Metakeywords'), 'fieldvalue' => null, 'class' => 'metainfo' ])
   
        </div>
        
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            @include('sgcontact::inputs.textarea', ['fieldname' => 'text', 'fieldlabel' => trans('sgcontact::sgcnt.Text'), 'fieldvalue' => null, 'class' => 'cktextarea' ])
        </div> 
    </div>

</div> 


@push('footer_scripts')
{!! Html::script('assets/ckeditor/ckeditor.js') !!}
<script>
    $(function () {
        $('.cktextarea').each(function (index) {
            CKEDITOR.replace($(this).attr('name'),{
                allowedContent: true,
                height: 600,
                filebrowserBrowseUrl : '/elfinder/ckeditor' });
        });


        $('.form-group').on('change', '#image', function(){
            var input = $(this)[0];
            var imgPreview = $(this).parent().parent().find('img');

            if ( input.files && input.files[0] ) {
                if ( input.files[0].type.match('image.*') ) {
                    var reader = new FileReader();
                    reader.onload = function(e) { $(imgPreview).attr('src', e.target.result); }
                    reader.readAsDataURL(input.files[0]);
                } else console.log('is not image mime type');
            } else console.log('not isset files data or files API not supordet');
            return true;
        });


    });
</script>
@endpush