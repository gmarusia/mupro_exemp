@extends('layouts.frontpage')

@section('content')
    <div class="container">
        @if (session('sendstatus'))
            <div class="alert alert-success">
                {{ session('sendstatus') }}
            </div>
        @endif

        <div class="row">
            <div class="col-sm-6 animated fadeInLeft animation-delay2">
                {!! $setting->text !!}
            </div>
            <div class="col-sm-6">
                {!! $setting->maps !!}
            </div>
        </div>

        <p>&nbsp;</p>
        <p>&nbsp;</p>

        <h2>@lang('sgcontact::sgcnt.Callback')</h2>
        <p>&nbsp;</p>
        {!! Form::open(['route' => 'sgcontact.sendmail' ]) !!}
        <div class="row sg-mailform">
            <div class="col-sm-6 col-md-5 col-md-offset-1">
                @include('sgcontact::inputs.text', ['fieldname' => 'name', 'fieldlabel' => trans('sgcontact::sgcnt.Name'), 'fieldvalue' => null, 'class' => 'sg-mailform-left' ])

                @include('sgcontact::inputs.email', ['fieldname' => 'email', 'fieldlabel' => trans('sgcontact::sgcnt.Email'), 'fieldvalue' => null, 'class' => 'sg-mailform-left' ])
                @include('sgcontact::inputs.text', ['fieldname' => 'phone', 'fieldlabel' => trans('sgcontact::sgcnt.Phone'), 'fieldvalue' => null , 'class' => 'sg-mailform-left'])
                @include('sgcontact::inputs.text', ['fieldname' => 'subject', 'fieldlabel' => trans('sgcontact::sgcnt.subjectSent'), 'fieldvalue' => null, 'class' => 'sg-mailform-left' ])

            </div>
            <div class="col-sm-6 col-md-5 col-md-offset-1 sg-mailform-right">
                @include('sgcontact::inputs.textarea', ['fieldname' => 'text', 'fieldlabel' => trans('sgcontact::sgcnt.Text'), 'fieldvalue' => null, 'class' => '', 'rows' => 10 ])
                <button type="submit" class="btn btn-primary">@lang('sgcontact::sgcnt.Send')</button>
            </div>

        </div>
        {!! Form::close() !!}

    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        function AnimatedCss() {
            var h = $(window).height();
            this.startAnimateScroll = function (params) {
                $(window).scroll(function () {
                    var obj = this;
                    $.each(params, function (key, val) {
                        if (($(obj).scrollTop() + h) >= $(val.divParent).offset().top ) {
                            $(val.divAnimate).css({visibility: "visible"});
                            $(val.divAnimate).addClass(val.animate + ' animated');
                        }
                        ;
                    });
                });
            };

        }
        ;
        $(window).load(function () {
            var paramsAnmCss = {
                obj1: { divParent: '.sg-mailform', divAnimate: '.sg-mailform-left', animate: 'fadeInLeft' },
                obj2: { divParent: '.sg-mailform', divAnimate: '.sg-mailform-right', animate: 'fadeInRight' }
            };
            var anmCss = new AnimatedCss();
            anmCss.startAnimateScroll(paramsAnmCss);
        });
    </script>
@endsection
