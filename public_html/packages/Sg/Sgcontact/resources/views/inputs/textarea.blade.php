<div class="form-group {!! isset($errors) && $errors->has($fieldname)?'has-error':'' !!} ">
    {!! Form::label($fieldname, $fieldlabel .': ') !!}
    {!! Form::textarea($fieldname, null, array('class'=>'form-control ' . $class, 'rows' => isset($rows)?$rows:4 )) !!}
    @if(isset($errors) && $errors->has($fieldname))
    <ul class="text-danger small">
        @foreach ($errors->get($fieldname) as $message)
        <li>{!! $message !!}</li>
        @endforeach
    </ul>
    @endif
</div> 
