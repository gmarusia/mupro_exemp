<?php

return [
    'ContactManager'        =>'Контакты',
    'Setting'               =>'Настройки',
    'Mails'                 =>'Почта',
    'Save'                  =>'Сохранить',
    'EmailReception'        =>'Емайл получателя',
    'Subject'               =>'Тема приходящего письма',
    'Text'                  =>'Текст',
    'MapsCode'              =>'Код карты',

    'Name'                  =>'Ваше имя',
    'Email'                 =>'Ваш емайл',
    'Phone'                 =>'Ваш контактный телефон',
    'subjectSent'           =>'Тема письма',
    'Callback'              =>'Обратная связь',
    'Send'                  =>'Отправить',



    /*
    'NewsGroups'            =>'Группы новостей',
    'NewsItems'             =>'Новости',
    'Add new Groups'        =>'Создать новую группу',
    'Add News'              =>'Создать новость',
    'Not groups'            =>'Нет групп',
    'Not news'              =>'Нет новостей',



    'Id'                    =>'Id',
    'NameGroup'             =>'Наименование группы',
    'Alias'                 =>'Алиас контента',
    'Decription'            =>'Описание группы',
    'Active'                =>'Активировано',
    'GroupName'             =>'Наименование группы',
    'GroupDescription'      =>'Описание группы',
    'GroupText'             =>'Текст для страницы группы',
    'PrefixClass'           =>'Префикс класса',
    'Title'                 =>'Заголовок',
    'OrderOut'              =>'Порядок отображения',
    'Text'                  => 'Текст',
    'ImageContent'          => 'Сопровождающее фото',
    'Metatitle'             =>'Мета заголовок',
    'Metadescription'       =>'Мета опис',
    'Metakeywords'          =>'Мета ключевые слова',


    'Create Group News'     =>'Создание группы новостей',
    'Create News'           =>'Создание новости',
    'Edit News'             =>'Редактирование новости',
    'ReadAllNews'           =>'Смотреть все новости',





    'ContentManager'        =>'Менеджер контента',
    'ContentGroups'         =>'Групы контента',
    'ContentItems'          =>'Контент',

    'Create Groups Content' =>'Создание новой группы контента',

    'Edit Groups Content'   =>'Редактирование группы контента',

    'IdArticle'             =>'Ід статьи', 
    'DecriptionItem'        =>'Описание контента',
     
    
    'Add Content Items'     =>'Добавить контент',
    'Create Items Content'  =>'Создание нового контента',

    'ArticleId'             =>'Ід контента', 
    'PreText'               =>'Текстовка перед заголовком',

    
    'Item'                  => 'Контент', 
    'Article'               => 'Контент', 
    'Parents'               => 'Родитель',
    
    'Edit Items Content'    => 'Редактирование контента',
    'SetFilter'             => 'Фильтровать',
    

    
    'readmore...'           => 'подробнее...',
    'readmore'              => 'Подробнее',
    

    */

    ''=>'',
];
