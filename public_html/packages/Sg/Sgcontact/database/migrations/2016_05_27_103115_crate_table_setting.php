<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateTableSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sg_sgcontact_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('activated')->default(TRUE);
            $table->string('reception', 255);
            $table->string('subject', 255);
            $table->string('logotip', 255);
            $table->text('text');
            $table->text('maps');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sg_sgcontact_setting');
    }
}
