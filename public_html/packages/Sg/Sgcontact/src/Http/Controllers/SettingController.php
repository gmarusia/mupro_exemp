<?php

namespace Sg\Sgcontact\Http\Controllers;

use App\Http\Controllers\Controller;
use Sg\Sgcontact\Models\Setting;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class SettingController extends Controller {

    protected $setting;

    public function __construct(Setting $setting) {
        $this->setting = $setting;
    }

    public function index() {
        return view('sgcontact::admin.setting.index')
                ->withSetting($this->setting->all());
    }

    /**
     * Форма создания новой
     */
    public function create() {
        return view('sgcontact::admin.setting.create');
    }

    /**
     * Запись из формы создания
     */
//    public function store(Request $request) {
//
//        $this->validate($request, Setting::$rules);
//
//        $this->setting->create($request->all());
//
//        return redirect(route('adminsc.sgcontact.setting.index'))
//                        ->with(['message' => trans('admin.Position Create'), 'alert-class' => 'alert-success']);
//    }

    /**
     * Display the specified resource. 
     */
    public function show($id) {
        return redirect(route('adminsc.sgcontact.setting.index'));
    }

    /**
     * Форма редактирования группы меню
     */
    public function edit($id) { 
        return view('sgcontact::admin.setting.edit', [ 'setting' => Setting::find($id)]);
    }

    /**
     * Запись формы редактирования
     */
    public function update(Request $request, $id) {
//        $rules = Setting::$rules;

        $setting = $this->setting->find($id);
        $setting->update($request->all());

        return redirect(route('adminsc.sgcontact.setting.edit', [ 'id' => $id ]))
            ->with(['message' => trans('admin.Position Update'), 'alert-class' => 'alert-success']);

    }

    /**
     * Удаление
     */
    public function destroy($id) {
        $this->setting->destroy($id);
        return redirect(route('adminsc.sgcontact.setting.index'))
                        ->with(['message' => trans('admin.Position Delete'), 'alert-class' => 'alert-success']);
    }

}
