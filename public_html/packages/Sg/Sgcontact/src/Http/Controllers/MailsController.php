<?php

namespace Sg\Sgcontact\Http\Controllers;

use App\Http\Controllers\Controller;
use Sg\Sgcontact\Models\Groups;
use Sg\Sgcontact\Models\News;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class MailsController extends Controller {

    protected $news;

    public function __construct(News $news) {
        $this->news = $news;
    }

    public function index(Request $request) {  
        return view('sgcontact::admin.news.index')
                ->withNews($this->news->paginate(25))
                ->withGroups(Groups::getGroupsListForSelect()) 
                ;
    }

    /**
     * Форма создания нового контента
     */
    public function create() {
        
        return view('sgcontact::admin.news.create')
                ->withGroups(Groups::getGroupsListForSelect()) 
                ->withOrderout($this->news->getCountByActivated() + 1)
                ;
    }

    /**
     * Запись из формы создания нового контента
     */
    public function store(Request $request) {
        $this->validate($request, News::$rules);
        $this->news->create($request->all());
        return redirect(route('adminsc.sgcontact.news.index'))
                        ->with(['message' => trans('admin.Position Create'), 'alert-class' => 'alert-success']);
    }

    /**
     * Display the specified resource.
     */
    public function show($id) {
        return redirect(route('adminsc.sgcontact.news.index'));
    }

    /**
     * Форма редактирования контента
     */
    public function edit($id) {
        return view('sgcontact::admin.news.edit')        
                ->withGroups(Groups::getGroupsListForSelect()) 
                ->withNews($this->news->FindOrFail($id))
                ; 
    }

    /**
     * Запись формы редактирования контента
     */
    public function update(Request $request, $id) { 
        $rules =  News::$rules ;
        $rules['alias'] = 'required|unique:sg_news,alias,' . $id ;
        
        $this->validate($request, $rules);

        $news = $this->news->FindOrFail($id);
        $news->update($request->all());

        return redirect(route('adminsc.sgcontact.news.index'))
                        ->with(['message' => trans('admin.Position Update'), 'alert-class' => 'alert-success']);
    }

    /**
     * Удаление
     */
    public function destroy($id) {
        $news = $this->news->FindOrFail($id);
        $news->delete();
        return redirect(route('adminsc.sgcontact.news.index'))
                        ->with(['message' => trans('admin.Position Delete'), 'alert-class' => 'alert-success']);
    }
    
    /**
     * Выдать статью по алиасу
     */
    public function getContentByAlias($alias) { 
        return view('sgcontact::frontpage.articles.article')
                ->withArticle($this->news->whereAlias($alias)->with('groups')->first()) 
                ;
    }
    
}
