<?php

namespace Sg\Sgcontact\Http\Controllers;

use App\Http\Controllers\Controller; 
use Sg\Sgcontact\Models\Mails;
use Sg\Sgcontact\Models\Setting;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request; 

class Fp_mailController extends Controller {

    protected $setting;

    public function __construct(Setting $setting) {
        $this->setting = $setting;
    }

    public function showFormMail() {
        return view('sgcontact::frontpage.mailform')
            ->withSetting(Setting::first());
    }

    public function sendFormMail(){
         return \Redirect::to(\URL::previous())->with('sendstatus' , 'Profile updated!' );
    }
}
