<?php

namespace Sg\Sgcontact;

use Illuminate\Support\ServiceProvider;

class SgcontactServiceProvider extends ServiceProvider {

    public function register() {
        $this->mergeConfigFrom(__DIR__ . '/../config/sgcontact.php', 'sgcontact');
        
        $this->app->bind('sgcontact', function () {
            return new Sgcontact;
        });
//        $this->app->bind('itemsevent', function () {
//            return new Lib\Itemsevent;
//        });
        
    }

    public function boot() {

        $this->publishes([__DIR__ . '/../config/sgcontact.php' => config_path('sgcontact.php')], 'config');
        $this->publishes([__DIR__ . '/../database/migrations' => $this->app->databasePath() . '/migrations'], 'migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'sgcontact');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang/', 'sgcontact');
        
//        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
//        $loader->alias('Itemsevent', 'Sg\Sgmenu\Facades\Itemsevent');

        require __DIR__ . '/Http/routes.php';
    }
     
}
