<?php

namespace Sg\Sgnews\Models;

use Illuminate\Database\Eloquent\Model; 

class Mails extends Model {

    protected $table = 'sg_news';
    public static $_pathImg = '/images/news/';

    protected $_uplFile ;  
    
    protected $fillable = array(    'pid', 
                                    'sg_news_group_id',
                                    'alias', 
                                    'pfxclass',
                                    'pretext', 
                                    'title',   
                                    'text',   
                                    'tags',
                                    'activated',  
                                    'image',
                                    'order',  
                                    'metatitle',
                                    'metadescription',
                                    'metakeywords',
                                    'published_at'
                                );
    
    protected $guarded = array('id', 'created_at', 'updated_at' );
      
    public static $rules = array(   'activated'         => 'required|numeric',
                                    'sg_news_group_id'=> 'required|numeric',
                                    'title'             => 'required',        
                                    'alias'             => 'required|unique:sg_news'
                                );
 
    public function groups() {
        return $this->belongsTo('Sg\Sgnews\Models\Groups', 'sg_news_group_id');
    }

    public function setAliasAttribute($value) {
        $this->attributes['alias'] = str_slug($value) ;
    }
  

    /**
     * Фильтровать по категории
     */
    public function scopeOfCategory($query, $catid) {
        if ($catid) {
            return $query->where('sg_news_group_id', '=', $catid);
        }
        return $query;
    }

    /**
     * Фильтровать по "включено"
     */
    public function scopeOfActivated($query) {
        return $query->where('activated', '=', TRUE );
    }      

    /**
     * Количество активных записей
     */
    public function getCountByActivated() {
        return (int)$this->OfActivated()->count();
    }
    
    /**
     * Реврайт метода с аплоад картинки
     */
    public static function create(array $attributes = array()) {
        if (isset($attributes['image']) && !empty($attributes['image']) ) {
            $file = $attributes['image'];
            $_fileName = str_random(3) . '_' . strtolower(str_replace(' ', '_', $file->getClientOriginalName())) ;
            $file->move(public_path(self::$_pathImg), $_fileName );
            $attributes['image'] = self::$_pathImg . $_fileName ;
        }
        return parent::create($attributes);
    }
    
    /**
     * Реврайт метода с аплоад картинки
     */
    public function update(array $attributes = [], array $options = []) {
        if (isset($attributes['image']) && !empty($attributes['image'])) {
            $file = $attributes['image'];
            $_fileName = str_random(3) . '_' . strtolower(str_replace(' ', '_', $file->getClientOriginalName()));

            if ($file->move(public_path(self::$_pathImg), $_fileName)) {
                $attributes['image'] = self::$_pathImg . $_fileName;
                \File::delete(public_path($this->image));
            } else {
                unset($attributes['image']);
            }
        } else {
            unset($attributes['image']);
        }
        return parent::update($attributes, $options);
    }

    /**
     * Реврайт метода с удалением картинки
     */
    public function delete()
    {
        \File::delete(public_path($this->image));
        return parent::delete();
    }

    /**
     * Все позиции по Id категории и активные
     */
    public static function getByIdCategory($idCat){
        return self::OfCategory($idCat)->ofActivated()->orderBy('order')->with('groups')->get();
    }
    
    /**
     * Получить статью по алиасу
     */
    public static function getNewsByAlias($alias){
        return self::ofActivated()
                    ->whereAlias($alias)
                    ->with('groups')
                    ->first();
    }

    /**
     * Вернуть отформатированную дату
     */
    /**
     * @return array
     */
    public function getPublishDate()
    {
        return Carbon::parse($this->published_at)->format('d.m.Y');
    }


     
}
