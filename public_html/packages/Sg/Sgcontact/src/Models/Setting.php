<?php

namespace Sg\Sgcontact\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

    protected $table = 'sg_sgcontact_setting';
    public static $_pathImg = '/images/sg_sgcontact_setting/';

    protected $_uplFile ;  
    
    protected $fillable = [ 'activated' ,
                            'reception',
                            'subject',
                            'logotip',
                            'text',
                            'maps'
                            ];
    
    protected $guarded = array('id', 'created_at', 'updated_at' );

    /**
     * Фильтровать по "включено"
     */
    public function scopeOfActivated($query) {
        return $query->where('activated', '=', TRUE );
    }      

    /**
     * Реврайт метода с аплоад картинки
     */
//    public static function create(array $attributes = array()) {
//        if (isset($attributes['image']) && !empty($attributes['image']) ) {
//            $file = $attributes['image'];
//            $_fileName = str_random(3) . '_' . strtolower(str_replace(' ', '_', $file->getClientOriginalName())) ;
//            $file->move(public_path(self::$_pathImg), $_fileName );
//            $attributes['image'] = self::$_pathImg . $_fileName ;
//        }
//        return parent::create($attributes);
//    }
    
    /**
     * Реврайт метода с аплоад картинки
     */
//    public function update(array $attributes = [], array $options = []) {
//        if (isset($attributes['image']) && !empty($attributes['image'])) {
//            $file = $attributes['image'];
//            $_fileName = str_random(3) . '_' . strtolower(str_replace(' ', '_', $file->getClientOriginalName()));
//
//            if ($file->move(public_path(self::$_pathImg), $_fileName)) {
//                $attributes['image'] = self::$_pathImg . $_fileName;
//                \File::delete(public_path($this->image));
//            } else {
//                unset($attributes['image']);
//            }
//        } else {
//            unset($attributes['image']);
//        }
//        return parent::update($attributes, $options);
//    }

    /**
     * Реврайт метода с удалением картинки
     */
//    public function delete()
//    {
//        \File::delete(public_path($this->image));
//        return parent::delete();
//    }

}
