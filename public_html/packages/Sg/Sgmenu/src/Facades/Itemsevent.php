<?php 
/**
 * Description of Mitems
 *
 * @author selezax
 */
 
namespace Sg\Sgmenu\Facades;
use Illuminate\Support\Facades\Facade;
 

class Itemsevent extends Facade{
    
    protected static function getFacadeAccessor() { 
        return 'itemsevent';         
    }
}
  
