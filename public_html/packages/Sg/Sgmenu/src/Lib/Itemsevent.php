<?php

namespace Sg\Sgmenu\Lib;

use Sg\Sgmenu\Models\Items;

class Itemsevent {

    public $MsgErr ;
   
    protected $_tree = [ 0 => '--------------------']; 
     
    /**
     * Получить дерево пунктов меню для select input
     */
    public function getItemsTreeArray() {
        $_cats = Items::select('id', 'pid', 'title')->orderBy('order')->OfActivated()->get()->toArray();
 
        $cats = array();
        foreach ($_cats as $cat) {
//            $cats_ID[$cat['id']][] = $cat;
            $cats[$cat['pid']][$cat['id']] = $cat;
        }
         
        $this->build_tree($cats, 0);
         
        return $this->_tree;
    }

    public function build_tree($cats, $pid, $marker = '') {
        if (is_array($cats) and isset($cats[$pid])) {
            $marker .= $marker;
            foreach ($cats[$pid] as $cat) {
                $this->_tree[$cat['id']] = $marker . $cat['title'];
                $this->build_tree($cats, $cat['id'], $marker . '-');
            }
        }
        return TRUE;
    }

    /**
     * Получить дерево пунктов меню для index view
     */
    public function getItemsTreeIndex($catid = null) {
        $_cats = Items::select('id', 'pid', 'title', 'order', 'type', 'activated', 'description', 'article_id')
                ->orderBy('order') 
                ->filterBySet()
                ->get()
                ->toArray();
        $cats = array();
        foreach ($_cats as $cat) { 
            $cats[$cat['pid']][$cat['id']] = $cat;
        }
        $this->build_itree($cats, 0);
        unset($this->_tree[0]);
        return $this->_tree;
    }

    public function build_itree($cats, $pid, $marker = '') {
        if (is_array($cats) and isset($cats[$pid])) {

            $marker .= $marker;

            foreach ($cats[$pid] as $cat) {

                $this->_tree[$cat['id']] = array(   'id'          => $cat['id'],
                                                    'pid'         => $cat['pid'],
                                                    'title'       => $marker . $cat['title'],
                                                    'order'       => $cat['order'],
                                                    'description' => $cat['description'],
                                                    'type'        => $cat['type'],
                                                    'activated'   => $cat['activated'],
                                                    'article_id'  => $cat['article_id'],
                                                );
                
                $this->build_itree($cats, $cat['id'], $marker . '— ');
            }
        } 
        return TRUE;
    }
 

////////////////////////////////////////////////////////////////////////////////
    static public function getFrontPageMenu($group_id) {
        return Items::ofEnabled()
                ->orderBy('order')
                ->with(array('langs' => function($query){
                                          $query->OfLang();
                                        })) 
                ->OfCategory($group_id)
                ->get()
                ->toArray(); 
    }

}
