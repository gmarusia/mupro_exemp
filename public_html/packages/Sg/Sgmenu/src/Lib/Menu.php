<?php

namespace Sg\Sgmenu\Lib;

use Sg\Sgmenu\Models\Groups;
//use Sg\Sgmenu\Models\Items;

class Menu {

    public $MsgErr;
    protected $menuData;
    public $groupData;
    protected $_textMenu;
    protected $_ClassUl = 'nav navbar-nav';
    protected $_currentLng;
    public $_countParent = 0 ;

    /**
     * Получить данные меню из конкретной группы
     */
    public function setDataMenuOfGroups($group_id) {
        $this->groupData = Groups::whereId((int)$group_id)
                ->with(['items' => function($q){    $q->ofActivated()
                                                    ->orderBy('order')  ;
                                                }
                        ])
                ->first()
                ->toArray()
        ;

        $this->menuData = $this->groupData['items'];
        return $this;
    }

    /**
     * Возвращает построенное меню
     */
    public function getReadyTreeMenu() {
        if (empty($this->_textMenu)) {
            throw new Exception('Menu text not ready');
        }
        return $this->_textMenu;
    }

    /**
     * Старт строительства меню
     */
    public function startMenu() {
        if (empty($this->menuData)) {
            throw new \Exception('Menu data is empty');
        }

        try {


            $this->_textMenu = '<ul class="' . $this->_ClassUl . '">';
            foreach ($this->getDataMenu(0) as $item) {
                $this->_textMenu .= $this->getMenuItemByType($item);
                $this->_countParent++;
            }
            $this->_textMenu .= '</ul>';
            return $this;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    /**
     * Получение данных из массива $menuData для пункта меню родителя
     */
    protected function getDataMenu($item) {
        $_item = array();
        foreach ($this->menuData as $key => $_im) {
            if ($_im['pid'] == $item) {
                $_item[] = $_im;
            }
        }

        return $_item;
    }

    /**
     * Текcтовка для элемента меню в зависимости от типа
     */
    protected function getMenuItemByType($item) {
        $_sectionMenu = '';

        switch ($item['type']) {
            case 1: // Пункт меню
//                $_sectionMenu = '<li><a href="/' . $this->_currentLng . $item['alias'] . '.html" title="' . $item['tag_alt'] . '" alt="' . $item['tag_alt'] . '" >' . $item['title'] . '</a></li>';
                $_sectionMenu = '<li><a href="' . route('sgmenu.' . $item['alias']) . '" title="' . $item['tag_alt'] . '" alt="' . $item['tag_alt'] . '" >' . $item['title'] . '</a></li>';
                break;

            case 2: // Заголовок для выпадающего меню
                $_sectionMenu = '
                       <li class="dropdown ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                ' . $item['title'] . '<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                ' . $this->BuildChildMenu($item['id']) . '
                            </ul>
                        </li> ';
                break;

            case 3: // Заголовок в меню
                $_sectionMenu = '<li class="dropdown-header">' . $item['title'] . '</li>';
                break;

            case 4: // Разделитель
                $_sectionMenu = '<li role="separator" class="divider"></li>';
                break;
        }
        return $_sectionMenu;
    }

    /**
     * Строительство childmenu в dropdawn
     */
    protected function BuildChildMenu($pid) {
        $_txttem = '';
        foreach ($this->getDataMenu($pid) as $item) {
            $_txttem .= $this->getMenuItemByType($item);
        }
        return $_txttem;
    }

}
