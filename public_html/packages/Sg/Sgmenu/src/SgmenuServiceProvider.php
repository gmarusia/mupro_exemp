<?php

namespace Sg\Sgmenu;

use Illuminate\Support\ServiceProvider;

class SgmenuServiceProvider extends ServiceProvider {

    public function register() {
        $this->mergeConfigFrom(__DIR__ . '/../config/sgmenu.php', 'sgmenu');
        
        $this->app->bind('sgmenu', function () {
            return new SgmenuF; 
        });
        $this->app->bind('itemsevent', function () {
            return new Lib\Itemsevent; 
        });
        
    }

    public function boot() {

        $this->publishes([__DIR__ . '/../config/sgmenu.php' => config_path('sgmenu.php')], 'config');
        $this->publishes([__DIR__ . '/../database/migrations' => $this->app->databasePath() . '/migrations'], 'migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'sgmenu');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang/', 'sgmenu');
        
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Itemsevent', 'Sg\Sgmenu\Facades\Itemsevent');

        require __DIR__ . '/Http/routes.php';
    }
     
}
