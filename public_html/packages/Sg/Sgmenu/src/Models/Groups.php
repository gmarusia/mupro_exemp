<?php

namespace Sg\Sgmenu\Models;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{

    protected $table = 'sg_sgmenu_groups';
    protected $fillable = ['activated', 'alias', 'title', 'description', 'pid'];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public static $rules = [    'title' => 'required|max:255',
                                'activated' => 'required|boolean',
                                'alias' => 'required|max:255|unique:sg_sgmenu_groups',
                            ];
    protected $casts = [ 'activated' => 'boolean',
                        ];

    public function items()
    {
        return $this->hasMany('Sg\Sgmenu\Models\Items', 'sg_sgmenu_group_id', 'id');
    }

    public function subcategories()
    {
        return $this->hasMany('Sg\Sgmenu\Models\Groups', 'pid');
    }

    /**
     * Преобразование alias
     */
    public function setAliasAttribute($value)
    {
        $this->attributes['alias'] = str_slug($value);
    }

    /**
     * Фильтровать по "включено"
     */
    public function scopeOfActivated($query)
    {
        return $query->where('activated', '=', true);
    }

    /**
     * Массив для input select
     */
    public static function getGroupsListForSelect()
    {
        return self::OfActivated()->lists('title', 'id');
    }

}
