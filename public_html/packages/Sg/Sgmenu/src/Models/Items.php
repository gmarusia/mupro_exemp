<?php

namespace Sg\Sgmenu\Models;

use Illuminate\Database\Eloquent\Model;

class Items extends Model {

    protected $table = 'sg_sgmenu_items';  
    
    protected $fillable = array(    'pid', 
                                    'sg_sgmenu_group_id',
                                    'alias',
                                    'alias_type',
                                    'pfxclass',
                                    'pretext', 
                                    'title',  
                                    'tag_alt',
                                    'description',   
                                    'tags',
                                    'activated',  
                                    'type', 
                                    'order', 
                                    'article_id', 
                                    'packets',
                                    'metatitle',
                                    'metadescription',
                                    'metakeywords'
                                );
    
    protected $guarded = array('id', 'created_at', 'updated_at' );
      
    public static $rules = array(    'activated'         => 'required|numeric',
                                    'sg_sgmenu_group_id'=> 'required|numeric',
                                    'title'             => 'required',        
                                    'alias'             => 'required',
                                    'alias_type'        => 'required|numeric',
                                    'type'              => 'required|numeric'
                                );
 
    public function groups() {
        return $this->belongsTo('Sg\Sgmenu\Models\Groups', 'sg_sgmenu_group_id');
    }

    public function setAliasAttribute($value) {
        $this->attributes['alias'] = str_slug($value) ;
    }

    /**
     * Список типов меню
     */
    public static function getTypsLink() {
        $_typeLinks = [ 0 => '-------------------',
            1 => trans('sgmenu::sgmenu.Item'),
            2 => trans('sgmenu::sgmenu.Item for dropdown'),
            3 => trans('sgmenu::sgmenu.Item for title'),
            4 => trans('sgmenu::sgmenu.Separator'),
                ];
        return $_typeLinks;
    }
    
    /**
     * Список типов aliasov
     */
    public static function getTypsAlias() {
        $_typeAlias = [ 0 => '-------------------',
            1 => trans('sgmenu::sgmenu.Article'),
            2 => trans('sgmenu::sgmenu.Packets') 
                ];
        return $_typeAlias;
    }

    /**
     * Фильтровать по категории
     */
    public function scopeOfCategory($query, $catid) {
        if ($catid) {
            return $query->where('sg_sgmenu_group_id', '=', $catid);
        }
        return $query;
    }

    /**
     * Фильтровать по "включено"
     */
    public function scopeOfActivated($query) {
        return $query->where('activated', '=', true);
    }      
    
    /**
     * Фильтровать по значению из сессии
     */
    public function scopeFilterBySet($query) { 
        if (\Session::has('sgmenu_items')) { 
            foreach(\Session::get('sgmenu_items') as $field => $value){ 
                $query->where($field, '=', $value);
            }  
        }
        return $query;
    }
    
    /**
     * Количество активных записей
     */
    public function getCountByActivated() {
        return (int)$this->OfActivated()->count();
    }
    
}
