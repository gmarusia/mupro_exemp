<?php

/**
 * workbench/sg/sgmenu/src/widgets/Wsgmenu.php
 * Sgmenu
 *
 * @package    Sg
 * @version    1.0.0
 * @author     Seleznyov A.
 * @copyright  (c) 2015
 * @link       selezax@gmail.com
 *
 * Обработка виджета меню
 */


namespace Sg\Sgmenu\Widgets;
use Sg\Sgmenu\Lib\Menu;

class Wsgmenu {

    private $_menus;

    /*
     *  Формирование меню
     */

    public function compose($view) {
        $viewdata = $view->getData();
        $menu = new Menu();


        if ($viewdata['type'] == 'tree') {
            $treeMenu = $menu->setDataMenuOfGroups($viewdata['group_id'])->startMenu()->getReadyTreeMenu();
            $view->with(['tree_menu' => $treeMenu, 'countParent' => $menu->_countParent, 'groupData' => $menu->groupData ]);
        }

        if ($viewdata['type'] == 'simple') {
            $view->with(['menu' => Itemsevent::getFrontPageMenu($viewdata['group_id']) ]);
        }
    }

}