<?php
/*
 * Виджет подготовки данных для вывода списка меню по ИД категории
 * 
 * Sg\Sgmenu
 * packages/Sg/Sgmenu/src/Widgets/Sgmenu.php
 * 
 * packages/Sg/Sgmenu/src/Models/Groups.php
 * 
 */

namespace Sg\Sgmenu\Widgets;

use Illuminate\Contracts\View\View;  
use Sg\Sgmenu\Models\Groups ;
use Illuminate\Support\Facades\Cache;

class Sgmenu {

    public function compose(View $view) {
        $viewdata = $view->getData();
        $view->with(['menu' => Cache::remember('sgmenu_idcat' . $viewdata['group_id'], 60, function() use($viewdata) {
                                                                                        return Groups::with('items')->find((int)$viewdata['group_id']);
                                                                                    }),
                    'title' => (isset($viewdata['title'])?$viewdata['title']:false)
                    ]);
    }

} 