<?php

//Route::group(['prefix' => 'sgmenu', 'namespace' => 'Sg\Sgmenu\Http\Controllers'], function () {
//
//    Route::get('/', ['as' => 'sgmenu.index', 'uses' => 'SgmenuController@index']);
//});

// Menu list    
Route::group(['namespace' => 'Sg\Sgmenu\Http\Controllers'], function () {
    $_route_list = Sg\Sgmenu\Models\Items::groupBy('alias')->whereActivated(TRUE)->whereType(1)->get()->toArray();
    foreach ($_route_list as $ritem) {
        Route::get($ritem['alias'] . '.html', array('as' => 'sgmenu.' . $ritem['alias'], function () use ($ritem) {
            if ($ritem['alias_type'] == 1) {

                if($ritem['alias'] == 'home'){
                    return view('frontpage.index');
                } else {
                    return App::make('Sg\Sgmenu\Http\Controllers\SgmenuController')->article($ritem['article_id']);
                }


            } elseif ($ritem['alias_type'] == 2) {
                list($_packetController, $_packetMethod) = explode('@', $ritem['packets']);
                return App::make($_packetController)->$_packetMethod($ritem['alias']);
            }
        }));
    }

    Route::get('testmenu', function(){
        return view('sgmenu::test');
    });

});

/* Administrator --------------------------------------------------------------- */
//Route::group([  'prefix' => 'adminsc/sgmenu',
//                'namespace' => 'Sg\Sgmenu\Http\Controllers',
//                'middleware' => ['authadmin']
//            ],
//
//    function () {
//        Route::resource('groups', 'GroupController');
//        Route::resource('items', 'ItemController');
//    });
