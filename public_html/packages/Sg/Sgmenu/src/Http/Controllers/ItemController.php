<?php

namespace Sg\Sgmenu\Http\Controllers;

use App\Http\Controllers\Controller;
use Sg\Sgmenu\Models\Groups;
use Sg\Sgmenu\Models\Items;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Sg\Sgmenu\Facades\Itemsevent;

class ItemController extends Controller {

    protected $items;

    public function __construct(Items $items) {
        $this->items = $items;
    }

     public function index(Request $request) { 
//        if( $request->has('group_id') ){ 
//            \Session::put( 'sgmenu_items.sg_sgmenu_group_id', $request->input('group_id') ); 
//        }
        
        return view('sgmenu::admin.items.index')
                ->withItems(Itemsevent::getItemsTreeIndex())
                ->withGroups(Groups::getGroupsListForSelect())
                ->withTypelink($this->items->getTypsLink())
                ->withTypealias($this->items->getTypsAlias())
                ;
    }

    /**
     * Форма создания нового пункта меню
     */
    public function create() {
        
        return view('sgmenu::admin.items.create')
                ->withGroups(Groups::getGroupsListForSelect())
                ->withTypelink($this->items->getTypsLink())
                ->withTypealias($this->items->getTypsAlias())
                ->withOrderout($this->items->getCountByActivated() + 1) 
                ;
    }

    /**
     * Запись из формы создания нового пункта меню
     */
    public function store(Request $request) {

        $this->validate($request, Items::$rules);
 
        $this->items->create($request->all());

        return redirect(route('adminsc.sgmenu.items.index'))
                        ->with(['message' => trans('admin.Position Create'), 'alert-class' => 'alert-success']);
    }


    public function show($id) {
        return redirect(route('adminsc.sgmenu.items.index'));
    }

    /**
     * Форма редактирования пункта меню
     */
    public function edit($id) {
        return view('sgmenu::admin.items.edit')        
                ->withGroups(Groups::getGroupsListForSelect())
                ->withTypelink($this->items->getTypsLink())
                ->withTypealias($this->items->getTypsAlias()) 
                ->withItem(Items::find($id))  
                ; 
    }

    /**
     * Запись формы редактирования меню
     */
    public function update(Request $request, $id) { 
         
        $this->validate($request, Items::$rules);

        $items = $this->items->find($id);
        $items->update($request->all());

        if($request->input('submit_ok') == 'submit_continue'){
            return redirect(route('adminsc.sgmenu.items.edit', [ 'id' => $id ]))
                ->with(['message' => trans('admin.Position Update'), 'alert-class' => 'alert-success']);
        }

        return redirect(route('adminsc.sgmenu.items.index'))
                        ->with(['message' => trans('admin.Position Update'), 'alert-class' => 'alert-success']);
    }

    /**
     * Удаление группы меню
     */
    public function destroy($id) {
        $this->items->destroy($id);
        return redirect(route('adminsc.sgmenu.items.index'))
                        ->with(['message' => trans('admin.Position Delete'), 'alert-class' => 'alert-success']);
    }
     
}
