<?php

namespace Sg\Sgmenu\Http\Controllers;

use App\Http\Controllers\Controller;
use Sg\Sgcontent\Models\Content;
use Illuminate\Support\Facades\Cache;

class SgmenuController extends Controller {

    public function index() {
        return response("Hello! This is Acl package.");
    }
    
    public function article($article_id) {
        return view('sgcontent::frontpage.articles.article')
                    ->withArticle(Cache::remember('sgcontent_articleID_' . $article_id, 240, function() use($article_id){
                                        return Content::with('groups')->find($article_id);
                                    })
                    );
    }

}
