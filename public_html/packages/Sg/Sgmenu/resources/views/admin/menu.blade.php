<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
       aria-expanded="false">@lang('sgmenu::admin.MenuManager')<span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
        <li class="{!! Request::is('adminsc/sgmenu/groups/*') || Request::path() == 'adminsc/sgmenu/groups'?'active':'' !!}">
            {!! link_to_route('adminsc.sgmenu.groups.index', trans('sgmenu::admin.MenuGroups')) !!}
        </li>
        <li class="{!! Request::is('adminsc/sgmenu/items/*') || Request::path() == 'adminsc/sgmenu/items'?'active':'' !!}">
            {!! link_to_route('adminsc.sgmenu.items.index', trans('sgmenu::admin.MenuItems')) !!}
        </li>
    </ul>
</li>
