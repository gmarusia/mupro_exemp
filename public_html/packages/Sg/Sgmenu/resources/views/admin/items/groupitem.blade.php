<div class="btn-group animated fadeInLeft" role="group" aria-label="..."  style="animation-delay: 0.6s;">
  <a href="{!! route('adminsc.sgmenu.groups.index') !!}" class="btn btn-primary btn-sm {!! $clsGroups or '' !!} ">@lang('sgmenu::sgmenu.MenuGroups')</a>
  <a href="{!! route('adminsc.sgmenu.items.index') !!}" class="btn btn-primary btn-sm {!! $clsItems or '' !!}">@lang('sgmenu::sgmenu.MenuItems')</a>
</div>
