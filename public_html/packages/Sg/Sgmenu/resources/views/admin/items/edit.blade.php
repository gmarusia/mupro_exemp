@extends('layouts.admin')

@section('metatitle')
    @lang('sgmenu::sgmenu.Edit Items Menu')
@stop

@section('page_header')
    @lang('sgmenu::sgmenu.Edit Items Menu')
@stop  

@section('tools_panel')
    {!! Form::model($item, array('method' => 'PATCH', 'route' => array('adminsc.sgmenu.items.update', $item->id))) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Update'), 'route' => 'adminsc.sgmenu.items.index', 'continue' => true ])
    @endsection
@endsection

@section('content') 

    @include('sgmenu::admin.items.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')
@stop
