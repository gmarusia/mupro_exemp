<div class="container">
    <div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#menuset" aria-controls="menuset" role="tab"
                                                      data-toggle="tab">@lang('sgmenu::sgmenu.MenuSetting')</a></li>
            <li role="presentation"><a href="#contentset" aria-controls="contentset" role="tab"
                                       data-toggle="tab">@lang('sgmenu::sgmenu.ContentSetting')</a></li>
            <li role="presentation"><a href="#metaset" aria-controls="metaset" role="tab"
                                       data-toggle="tab">@lang('sgmenu::sgmenu.MetaSetting')</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            {{-- ------------------------------------------------------------------------------------------------------- --}}
            <div role="tabpanel" class="tab-pane fade in  active" id="menuset">
                <p>&nbsp</p>
                <div class="row">
                    <div class="col-sm-5">
                        @include('admin.inputs.onoff', ['fieldname' => 'activated', 'fieldlabel' => trans('sgmenu::sgmenu.Activated'), 'fieldvalue' => isset($item)?$item->activated:1 ])

                        @include('admin.inputs.inporder', [ 'fieldvalue' => isset($orderout)?$orderout:$item->order])

                        @include('sgmenu::inputs.text', ['fieldname' => 'alias', 'fieldlabel' => trans('sgmenu::sgmenu.Alias'), 'fieldvalue' => null ])
                        @include('sgmenu::inputs.text', ['fieldname' => 'pretext', 'fieldlabel' => trans('sgmenu::sgmenu.PreText'), 'fieldvalue' => null ])
                        <hr/>
                        @include('sgmenu::inputs.select', ['fieldname' => 'sg_sgmenu_group_id', 'fieldlabel' => trans('sgmenu::sgmenu.NameGroup'), 'fieldvalue' => null, 'arrayDate' => $groups ])
                        @include('sgmenu::inputs.select', ['fieldname' => 'pid', 'fieldlabel' => trans('sgmenu::sgmenu.Parents'), 'fieldvalue' => null, 'arrayDate' => Itemsevent::getItemsTreeArray() ])

                    </div>

                    <div class="col-sm-5 col-sm-offset-1">
                        @include('sgmenu::inputs.text', ['fieldname' => 'title', 'fieldlabel' => trans('sgmenu::sgmenu.Title'), 'fieldvalue' => null ])
                        @include('sgmenu::inputs.text', ['fieldname' => 'tag_alt', 'fieldlabel' => trans('sgmenu::sgmenu.Alt'), 'fieldvalue' => null ])
                        @include('sgmenu::inputs.textarea', ['fieldname' => 'description', 'fieldlabel' => trans('sgmenu::sgmenu.DecriptionItem'), 'fieldvalue' => null ])
                    </div>
                </div>
            </div>

            {{-- ------------------------------------------------------------------------------------------------------- --}}
            <div role="tabpanel" class="tab-pane fade" id="contentset">
                <p>&nbsp</p>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        @include('sgmenu::inputs.select', ['fieldname' => 'type', 'fieldlabel' => trans('sgmenu::sgmenu.TypeItems'), 'fieldvalue' => null, 'arrayDate' => $typelink ])

                        @include('sgmenu::inputs.select', ['fieldname' => 'alias_type', 'fieldlabel' => trans('sgmenu::sgmenu.TypeAlias'), 'fieldvalue' => null, 'arrayDate' => $typealias ])

                        @include('sgmenu::inputs.text', ['fieldname' => 'article_id', 'fieldlabel' => trans('sgmenu::sgmenu.ArticleId'), 'fieldvalue' => null ])

                        @include('sgmenu::inputs.text', ['fieldname' => 'packets', 'fieldlabel' => trans('sgmenu::sgmenu.Packets'), 'fieldvalue' => null ])
                    </div>
                </div>
            </div>

            {{-- ------------------------------------------------------------------------------------------------------- --}}
            <div role="tabpanel" class="tab-pane fade" id="metaset">
                <p>&nbsp</p>
                <div class="row">
                    <div class="col-sm-5">
                        @include('sgmenu::inputs.text', ['fieldname' => 'metatitle', 'fieldlabel' => trans('sgmenu::sgmenu.Metatitle'), 'fieldvalue' => null ])
                    </div>

                    <div class="col-sm-5 col-sm-offset-1">
                        @include('sgmenu::inputs.textarea', ['fieldname' => 'metadescription', 'fieldlabel' => trans('sgmenu::sgmenu.Metadescription'), 'fieldvalue' => null ])

                        @include('sgmenu::inputs.textarea', ['fieldname' => 'metakeywords', 'fieldlabel' => trans('sgmenu::sgmenu.Metakeywords'), 'fieldvalue' => null ])
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

</div>
