@extends('layouts.admin')

@section('metatitle')
    @lang('sgmenu::sgmenu.MenuGroups')
@stop

@section('page_header')
    @lang('sgmenu::sgmenu.MenuGroups')
@stop


@section('tools_panel')
    @parent
@section('in_tools_panel')
    @include('sgmenu::admin.items.groupitem', [ 'clsItems' =>'active', 'clsGroups' => '' ])
    @include('admin.inputs.toolsbutton_add', [ '_add_link' => route('adminsc.sgmenu.items.create'), 'btn_label' => trans('sgmenu::sgmenu.Add Menu Items') ] )
@endsection
@endsection


@section('content') 

{!! Form::open([ 'method' => 'GET', 'route' => ['adminsc.sgmenu.items.index'], 'class' => "form-horizontal" ]) !!}
              
  <div class="form-group">
    {!! Form::label( 'group_id', trans('sgmenu::sgmenu.NameGroup') . ': ', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-3">
      {!! Form::select('group_id', $groups, \Session::has('sgmenu_items.sg_sgmenu_group_id')?\Session::get('sgmenu_items.sg_sgmenu_group_id'):null, array('class'=>'form-control')) !!} 
    </div>
    {!! Form::submit(trans('sgmenu::sgmenu.SetFilter'), array('class' => 'btn btn-warning btn-sm')) !!} 
  </div>

{!! Form::close() !!}

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>@lang('sgmenu::sgmenu.Id')</th>
            <th>@lang('sgmenu::sgmenu.Title')</th> 
            <th>@lang('sgmenu::sgmenu.Decription')</th>  
            <th>@lang('sgmenu::sgmenu.TypeItems')</th> 
            <th>@lang('sgmenu::sgmenu.OrderOut')</th> 
            <th>@lang('sgmenu::sgmenu.IdArticle')</th>  
            <th>@lang('sgmenu::sgmenu.Active')</th> 
            <th> </th>
            <th> </th>
        </tr>
    </thead>
    
    @forelse($items as $item)
        <tr>
            <td>{!! $item['id'] !!}</td>
            <td>{!! $item['title'] !!}</td> 
            <td>{!! $item['description'] !!}</td> 
            <td class="text-center">{!! $typelink[$item['type']] !!}</td> 
            <td class="text-center">{!! $item['order'] !!}</td> 
            <td class="text-center">{!! $item['article_id'] == 0?'':$item['article_id'] !!}</td> 
            <td>
                @include('admin.inputs.small.tbl_icon_onoff', [ 'marker' => $item['activated'] ])
            </td> 
            <td class="text-right">
                @include('admin.inputs.small.tbl_btn_edit', [ 'link' => route('adminsc.sgmenu.items.edit',[ 'id' => $item['id'] ]) ])
            </td>
            <td>
                @include('admin.inputs.small.tbl_btn_delete', [ 'link' => route('adminsc.sgmenu.items.destroy',[ 'id' => $item['id'] ]) ])
            </td>    
        </tr>
    @empty
        <tr>
            <td colspan="6" >@lang('sgmenu::sgmenu.Not items')</td>
        </tr>
    @endforelse    
     
    <tbody>    
</table>

    

@stop
