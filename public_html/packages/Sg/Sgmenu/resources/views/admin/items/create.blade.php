@extends('layouts.admin')

@section('metatitle')
    @lang('sgmenu::sgmenu.Create Items Menu')
@stop

@section('page_header')
    @lang('sgmenu::sgmenu.Create Items Menu')
@stop  

@section('tools_panel')
    {!! Form::open(array('route' => 'adminsc.sgmenu.items.store' )) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Create'), 'route' => 'adminsc.sgmenu.items.index' ])
    @endsection
@endsection



@section('content') 

    @include('sgmenu::admin.items.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')
@stop
