<div class="row">
    <div class="col-md-4 col-md-offset-4">

        @include('admin.inputs.onoff', ['fieldname' => 'activated', 'fieldlabel' => trans('sgmenu::sgmenu.Activated'), 'fieldvalue' => isset($group)?$group->activated:1 ])
         
        @include('sgmenu::inputs.text', ['fieldname' => 'alias', 'fieldlabel' => trans('sgmenu::sgmenu.AliasGroup'), 'fieldvalue' => null ])
        @include('sgmenu::inputs.text', ['fieldname' => 'title', 'fieldlabel' => trans('sgmenu::sgmenu.GroupName'), 'fieldvalue' => null ])

        @include('sgmenu::inputs.textarea', ['fieldname' => 'description', 'fieldlabel' => trans('sgmenu::sgmenu.GroupDescription'), 'fieldvalue' => null ])

    </div>
</div> 
