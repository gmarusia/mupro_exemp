@extends('layouts.admin')

@section('metatitle')
    @lang('sgmenu::sgmenu.Edit Groups Menu')
@stop

@section('page_header')
    @lang('sgmenu::sgmenu.Edit Groups Menu')
@stop  



@section('tools_panel')
    {!! Form::model($group, array('method' => 'PATCH', 'route' => array('adminsc.sgmenu.groups.update', $group->id))) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Update'), 'route' => 'adminsc.sgmenu.groups.index', 'continue' => true ])
    @endsection
@endsection


@section('content') 

    @include('sgmenu::admin.groups.fields')

    {!! Form::close() !!}

    @include('admin.inputs.errorlist')

@stop
