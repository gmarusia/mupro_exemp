@extends('layouts.admin')

@section('metatitle')
    @lang('sgmenu::sgmenu.Create Groups Menu')
@stop

@section('page_header')
    @lang('sgmenu::sgmenu.Create Groups Menu')
@stop  

@section('tools_panel')
    {!! Form::open(array('route' => 'adminsc.sgmenu.groups.store' )) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Create'), 'route' => 'adminsc.sgmenu.groups.index' ])
    @endsection
@endsection

@section('content') 

    @include('sgmenu::admin.groups.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')
@stop
