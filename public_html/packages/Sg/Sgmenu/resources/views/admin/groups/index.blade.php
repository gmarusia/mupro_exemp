@extends('layouts.admin')

@section('metatitle')
    @lang('sgmenu::sgmenu.MenuGroups')
@stop

@section('page_header')
    @lang('sgmenu::sgmenu.MenuGroups')
@stop


@section('tools_panel')
    @parent
    @section('in_tools_panel')
        @include('sgmenu::admin.items.groupitem', [ 'clsItems' =>'', 'clsGroups' => 'active' ])
        @include('admin.inputs.toolsbutton_add', [ '_add_link' => route('adminsc.sgmenu.groups.create'), 'btn_label' => trans('sgmenu::sgmenu.Add new Groups') ] )
    @endsection
@endsection

   


@section('content') 


<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>@lang('sgmenu::sgmenu.Id')</th>
            <th>@lang('sgmenu::sgmenu.AliasGroup')</th>
            <th>@lang('sgmenu::sgmenu.NameGroup')</th>
            <th>@lang('sgmenu::sgmenu.Decription')</th>
            <th>@lang('sgmenu::sgmenu.Active')</th> 
            <th> </th>
            <th> </th>
        </tr>
    </thead>
    
    @forelse($groups as $group)
        <tr>
            <td>{!! $group->id !!}</td>
            <td>{!! $group->alias !!}</td>
            <td>{!! $group->title !!}</td>
            <td>{!! $group->description !!}</td>
            <td>
                @include('admin.inputs.small.tbl_icon_onoff', [ 'marker' => $group->activated ])
            </td> 
            <td class="text-right">
                @include('admin.inputs.small.tbl_btn_edit', [ 'link' => route('adminsc.sgmenu.groups.edit',[ 'id' => $group->id]) ])
            </td>
            <td>
                @include('admin.inputs.small.tbl_btn_delete', [ 'link' => route('adminsc.sgmenu.groups.destroy',[ 'id' => $group->id]) ])
            </td>    
        </tr>
    @empty
        <tr>
            <td colspan="6" >@lang('sgmenu::sgmenu.Not groups')</td>
        </tr>
    @endforelse    
     
    <tbody>    
</table>

    

@stop
