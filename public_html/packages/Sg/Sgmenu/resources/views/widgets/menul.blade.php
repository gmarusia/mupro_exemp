<?php
/**
 * Темплейт для вывода списка меню по ИД категории
 * 
 * Sg\Sgmenu
 * packages/Sg/Sgmenu/resources/views/widgets/menul.blade.php
 * 
 * packages/Sg/Sgmenu/src/Widgets/Sgmenu.php * 
 */
?>

{{-- <style>
    .top-menu-item{
        width: {{ 100 / $menu->items->count() }}%;
    }
</style> --}}

@foreach($menu->items as $item)
    <li class="top-menu-item {!! Request::is('adminsc/admins/*') || Request::path() == $item->alias . '.html'?'active':'' !!}">
        <a href="{{ route('sgmenu.' . $item->alias) }}" >{{ $item->title }}</a>
    </li>
@endforeach