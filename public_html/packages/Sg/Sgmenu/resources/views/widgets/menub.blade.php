<?php
/**
 * Темплейт для вывода списка меню по ИД категории
 * 
 * Sg\Sgmenu
 * packages/Sg/Sgmenu/resources/views/widgets/menul.blade.php
 * 
 * packages/Sg/Sgmenu/src/Widgets/Sgmenu.php * 
 
 */
 
?>
<div class="sgmenu {!! $menu->alias !!}">
    @if($title)
        <h3>{!! $menu->title !!}</h3>
    @endif
    <ul >
        @foreach($menu->items as $item)
            <li>
                <a href="{{ route('sgmenu.' . $item->alias) }}" >
                    {{ $item->title }}
                </a>
            </li>
        @endforeach
    </ul>
</div>