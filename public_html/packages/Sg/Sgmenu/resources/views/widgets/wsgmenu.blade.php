<?php

/**
 * workbench/sg/sgmenu/src/views/widgets/wsgmenu.blade.php
 * Sgmenu
 *
 * @package    Sg
 * @version    1.0.0
 * @author     Seleznyov A.
 * @copyright  (c) 2015
 * @link       selezax@gmail.com
 *
 * Шаблон для вывода виджета меню в виде дерева
 */
?>

<style>
    div#navbar > ul > li {
        width: {{ 100 / $countParent }}%;
    }
</style>

<nav class="navbar navbar-default {{$groupData['alias']}}">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{--<a class="navbar-brand" href="/">@lang('fp.SiteName')</a>--}}
        </div>
        <div id="navbar" class="navbar-collapse collapse {{$groupData['alias']}}">
            {!! $tree_menu !!}
        </div>
    </div>
</nav>