<div class="form-group {!! $errors->has($fieldname)?'has-error':'' !!} ">
    {!! Form::label($fieldname, $fieldlabel .': ') !!}
    {!! Form::select($fieldname, $arrayDate, null, array('class'=>'form-control')) !!}
    @if($errors->has($fieldname))
    <ul class="text-danger small">
        @foreach ($errors->get($fieldname) as $message)
        <li>{!! $message !!}</li>
        @endforeach
    </ul>
    @endif
</div>
