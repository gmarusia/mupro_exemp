<?php

return [  
    
    'MenuManager'       =>'Менеджер меню',
    'MenuGroups'        =>'Групы меню',
    'MenuItems'         =>'Пункты меню',
    
    'Add new Groups'    =>'Создать новую группу',
    'Create Groups Menu'=>'Создание новой группы меню',
    'GroupName'         =>'Наименование группы',
    'GroupDescription'  =>'Описание группы',
    'Edit Groups Menu'  =>'Редактирование группы меню',
    
    'Id'                =>'Id',
    'NameGroup'         =>'Найменування групи',
    'Decription'        =>'Описание группы',
    'Active'            =>'Активировано',
    'Title'             =>'Заголовок',
    'TypeItems'         =>'Тип пункта меню',
    'OrderOut'          =>'Порядок вывода',
    'IdArticle'         =>'Ид статьи', 
    'DecriptionItem'    =>'Описание меню',
     
    
    'Add Menu Items'    =>'Добавить пункт меню',
    'Create Items Menu' =>'Создание нового пункта меню',
    
    'Alias'             =>'Аліас меню',
    'ArticleId'         =>'Ід контента',
    'Packets'           =>'Пакет (Controller@method)',
    'PreText'           =>'Текстовка перед заголовком',
    
    'Metatitle'         =>'Мета заголовок', 
    'Metadescription'   =>'Мета описание',
    'Metakeywords'      =>'Мета ключевые слова',
    
    'Item'              => 'Пункт меню',
    'Item for dropdown' => 'Заголовок для випадающего списка',
    'Separator'         => 'Разделитель',
    'Item for title'    => 'Заголовок в меню',  
    'Article'           => 'Контент',
    'Alt'               => 'alt, title ',
    'Parents'           => 'Родитель',
    
    'Edit Items Menu'   => 'Редактирование пункта меню',
    'SetFilter'         => 'Фильтровать',
    
    ''=>'',
];
