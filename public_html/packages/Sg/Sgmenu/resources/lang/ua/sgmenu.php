<?php

return [  
    
    'MenuManager'       =>'Менеджер меню',
    'MenuGroups'        =>'Групи меню',
    'MenuItems'         =>'Пункти меню',
    
    'Add new Groups'    =>'Створити нову группу',
    'Create Groups Menu'=>'Створення нової групи меню',
    'GroupName'         =>'Найменування групи',
    'GroupDescription'  =>'Опис групи',
    'Edit Groups Menu'  =>'Редагування групи меню',
    
    'Id'                =>'Id',
    'NameGroup'         =>'Найменування групи',
    'Decription'        =>'Опис групи',
    'Active'            =>'Активовано',
    'Title'             =>'Заголовок',
    'TypeItems'         =>'Тип пункту меню',
    'OrderOut'          =>'Порядок виведення',
    'IdArticle'         =>'Ід статті', 
    'DecriptionItem'    =>'Опис меню',
     
    
    'Add Menu Items'    =>'Додати пункт меню',
    'Create Items Menu' =>'Створення нового пункту меню',
    
    'Alias'             =>'Аліас меню',
    'ArticleId'         =>'Ід контента',
    'Packets'           =>'Пакет (Controller@method)',
    'PreText'           =>'Текстовка перед заголовком',
    
    'Metatitle'         =>'Мета заголовок', 
    'Metadescription'   =>'Мета опис',
    'Metakeywords'      =>'Мета ключові слова',
    
    'Item'              => 'Пункт меню',
    'Item for dropdown' => 'Заголовок для випадаючого списку',
    'Separator'         => 'Роздільник',
    'Item for title'    => 'Заголовок в меню',  
    'Article'           => 'Контент',
    'Alt'               => 'alt, title ',
    'Parents'           => 'Родитель',
    
    'Edit Items Menu'   => 'Редагування пункту меню',
    'SetFilter'         => 'Фільтрувати',
    
    ''=>'',
];
