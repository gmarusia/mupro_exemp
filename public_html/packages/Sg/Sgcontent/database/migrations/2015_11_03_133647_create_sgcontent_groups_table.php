<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSgcontentGroupsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sg_sgcontent_groups', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('pid')->nullable();
            $table->string('pfxclass');
            $table->string('alias');
            $table->string('title');
            $table->text('text');
            $table->text('description');
            $table->boolean('activated')->default(TRUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sg_sgcontent_groups');
    }

}
