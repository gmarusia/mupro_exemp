<?php

return [  
    
    'ContentManager'        =>'Менеджер контента',
    'ContentGroups'         =>'Групы контента',
    'ContentItems'          =>'Контент',
    
    'Add new Groups'        =>'Создать новую группу',
    'Create Groups Content' =>'Создание новой группы контента',
    'GroupName'             =>'Наименование группы',
    'GroupDescription'      =>'Описание группы',
    'GroupText'             =>'Текст для страницы группы',
    'Edit Groups Content'   =>'Редактирование группы контента',
    
    'Id'                    =>'Id',
    'NameGroup'             =>'Наименование группы',
    'Decription'            =>'Описание группы',
    'Active'                =>'Активировано',
    'Title'                 =>'Заголовок', 
    'OrderOut'              =>'Порядок отображения',
    'IdArticle'             =>'Ід статьи', 
    'DecriptionItem'        =>'Описание контента',
     
    
    'Add Content Items'     =>'Добавить контент',
    'Create Items Content'  =>'Создание нового контента',
    
    'Alias'                 =>'Алиас контента',
    'ArticleId'             =>'Ід контента', 
    'PreText'               =>'Текстовка перед заголовком',
    
    'Metatitle'             =>'Мета заголовок', 
    'Metadescription'       =>'Мета опис',
    'Metakeywords'          =>'Мета ключевые слова',
    
    'Item'                  => 'Контент', 
    'Article'               => 'Контент', 
    'Parents'               => 'Родитель',
    
    'Edit Items Content'    => 'Редактирование контента',
    'SetFilter'             => 'Фильтровать',
    
    'PrefixClass'           => 'Префикс класса',
    'Text'                  => 'Текст',
    
    'ImageContent'          => 'Сопровождающее фото',
    
    'readmore...'           => 'подробнее...',
    'readmore'              => 'Подробнее',
    
    
    'Not groups'            =>'Нет групп',
    'Not contents'          =>'Нет контента',
    ''=>'',
];
