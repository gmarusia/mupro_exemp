<?php

return [  
    
    'ContentManager'        =>'Менеджер контенту',
    'ContentGroups'         =>'Групи контенту',
    'ContentItems'          =>'Контент',
    
    'Add new Groups'        =>'Створити нову группу',
    'Create Groups Content' =>'Створення нової групи контенту',
    'GroupName'             =>'Найменування групи',
    'GroupDescription'      =>'Опис групи',
    'Edit Groups Content'   =>'Редагування групи контенту',
    
    'Id'                    =>'Id',
    'NameGroup'             =>'Найменування групи',
    'Decription'            =>'Опис групи',
    'Active'                =>'Активовано',
    'Title'                 =>'Заголовок', 
    'OrderOut'              =>'Порядок виведення',
    'IdArticle'             =>'Ід статті', 
    'DecriptionItem'        =>'Опис контенту',
     
    
    'Add Content Items'     =>'Додати контент',
    'Create Items Content'  =>'Створення нового контенту',
    
    'Alias'                 =>'Аліас контенту',
    'ArticleId'             =>'Ід контента', 
    'PreText'               =>'Текстовка перед заголовком',
    
    'Metatitle'             =>'Мета заголовок', 
    'Metadescription'       =>'Мета опис',
    'Metakeywords'          =>'Мета ключові слова',
    
    'Item'                  => 'Контент', 
    'Article'               => 'Контент', 
    'Parents'               => 'Родитель',
    
    'Edit Items Content'    => 'Редагування контенту',
    'SetFilter'             => 'Фільтрувати',
    
    'PrefixClass'           => 'Префікс класу',
    'Text'                  => 'Текст',
    
    
    ''=>'',
];
