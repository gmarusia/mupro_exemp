@extends('layouts.frontpage')

@section('metatitle'){{ $article->metatitle }} @stop
@section('metadescription'){{ $article->metadescription }} @stop
@section('metakeywords'){{ $article->metakeywords }} @stop

@section('content') 
<div class="main-text-info {{ $article->pfxclass }}">
    
    <p>&nbsp;</p>
    {{-- --------------------------------------------------------------- --}}
    {!! View::make('sgcontent::widgets.contentblogdecoration', [ 'alias' => 'protsedures', 'title' => false ] ) !!}
    {{-- --------------------------------------------------------------- --}}  
 
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {!! $article->title !!}
            </div>
        </div>
        <div class="text"> 
            {!! $article->text !!} 
        </div>

    </div>    
</div>    
@stop

@section('footer')
{!! Html::script('/assets/js/all.js') !!}
<script type="text/javascript">
    $(window).load(function () {
        var paramsAnmCss = {obj1: {divParent: '.procedure-presentation #proc_item1', divAnimate: '.procedure-presentation .proc-item', animate: 'zoomIn'}
        };
        var anmCss = new AnimatedCss();
        anmCss.startAnimateScroll(paramsAnmCss);
    });
</script> 
@stop