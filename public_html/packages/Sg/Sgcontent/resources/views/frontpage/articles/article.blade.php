@extends('layouts.frontpage')

@section('metatitle')
    {!! $article->title !!} - {!! $article->groups->title !!}
@stop


@section('content')
    <div class="container">

        <div class="article{!! $article->pfxclass !!}">
            <?php /*  ?><h1>
        {!! $article->title !!}
    </h1><?php */  ?>

            <div class="article-text">
                {!! $article->text !!}
            </div>
        </div>
    </div>

@stop
