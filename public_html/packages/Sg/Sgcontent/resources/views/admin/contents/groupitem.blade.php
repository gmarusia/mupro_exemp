<div class="btn-group" role="group" aria-label="..."> 
  <a href="{!! route('adminsc.sgcontent.groups.index') !!}" class="btn btn-primary btn-sm {!! $clsGroups or '' !!} ">@lang('sgcontent::sgcontent.ContentGroups')</a>
  <a href="{!! route('adminsc.sgcontent.contents.index') !!}" class="btn btn-primary btn-sm {!! $clsContents or '' !!}">@lang('sgcontent::sgcontent.ContentItems')</a>
</div>
