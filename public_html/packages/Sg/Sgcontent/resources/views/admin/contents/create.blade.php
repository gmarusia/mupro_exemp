@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontent::sgcontent.Create Items Content')
@stop

@section('page_header')
    @lang('sgcontent::sgcontent.Create Items Content')
@stop  


@section('tools_panel')
    {!! Form::open(array('route' => 'adminsc.sgcontent.contents.store', 'files'=>true )) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Create'), 'route' => 'adminsc.sgcontent.contents.index' ])
    @endsection
@endsection


@section('content') 

    @include('sgcontent::admin.contents.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')
@stop

@section('footer') 

@stop                
