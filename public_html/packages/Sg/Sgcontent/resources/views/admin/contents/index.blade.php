@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontent::sgcontent.ContentManager')
@stop

@section('page_header')
    @lang('sgcontent::sgcontent.ContentManager')
@stop


@section('tools_panel')
    @parent
    @section('in_tools_panel')
        @include('sgcontent::admin.contents.groupitem', [ 'clsContents' =>'active', 'clsGroups' => '' ])
        @include('admin.inputs.toolsbutton_add', [ '_add_link' => route('adminsc.sgcontent.contents.create'), 'btn_label' => trans('sgcontent::sgcontent.Add Content Items') ] )
    @endsection
@endsection


@section('content') 

@include('sgcontent::admin.contents.setfilter')


<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>@lang('sgcontent::sgcontent.Id')</th>
            <th>@lang('sgcontent::sgcontent.Title')</th> 
            <th>@lang('sgcontent::sgcontent.Alias')</th> 
            <th>@lang('sgcontent::sgcontent.OrderOut')</th>  
            <th>@lang('sgcontent::sgcontent.Active')</th> 
            <th> </th>
            <th> </th>
        </tr>
    </thead>
    
    @forelse($contents as $item)
        <tr>
            <td>{!! $item['id'] !!}</td>
            <td>{!! $item['title'] !!}</td>  
            <td>{!! $item['alias'] !!}</td>  
            <td class="text-center">{!! $item['order'] !!}</td>  
            <td>
                @include('admin.inputs.small.tbl_icon_onoff', [ 'marker' => $item->activated ])
            </td> 
            <td class="text-right">
                @include('admin.inputs.small.tbl_btn_edit', [ 'link' => route('adminsc.sgcontent.contents.edit',[ 'id' => $item->id]) ])
            </td>
            <td>
                @include('admin.inputs.small.tbl_btn_delete', [ 'link' => route('adminsc.sgcontent.contents.destroy',[ 'id' => $item->id]) ])
            </td>    
        </tr>
    @empty
        <tr>
            <td colspan="6" >@lang('sgcontent::sgcontent.Not contents')</td>
        </tr>
    @endforelse    
     
    <tbody>    
</table>

    

@stop
