{!! Form::open([ 'method' => 'GET', 'route' => ['adminsc.sgcontent.contents.index'], 'class' => "form-horizontal" ]) !!}

<div class="form-group">
    {!! Form::label( 'group_id', trans('sgcontent::sgcontent.NameGroup') . ': ', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-3">
        {!! Form::select('group_id', $groups, \Session::has('sgcontent_contents.sg_sgcontent_group_id')?\Session::get('sgcontent_contents.sg_sgcontent_group_id'):null, array('class'=>'form-control')) !!} 
    </div>
    {!! Form::submit(trans('sgcontent::sgcontent.SetFilter'), array('class' => 'btn btn-warning btn-sm')) !!}      
</div>

{!! Form::close() !!}
