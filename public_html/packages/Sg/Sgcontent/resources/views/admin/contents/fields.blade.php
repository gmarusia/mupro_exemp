<div class="container">
    <div class="row">
        <div class="col-md-4 text-primary ">
            @include('admin.inputs.onoff', ['fieldname' => 'activated', 'fieldlabel' => trans('sgmenu::sgmenu.Activated'), 'fieldvalue' => isset($content)?$content->activated:1 ])

            @include('admin.inputs.inporder', [ 'fieldvalue' => isset($orderout)?$orderout:$content->order])


            @include('sgcontent::inputs.select', ['fieldname' => 'sg_sgcontent_group_id', 'fieldlabel' => trans('sgcontent::sgcontent.NameGroup'), 'fieldvalue' => null, 'arrayDate' => $groups ])             
            @include('sgcontent::inputs.text', ['fieldname' => 'pfxclass', 'fieldlabel' => trans('sgcontent::sgcontent.PrefixClass'), 'fieldvalue' => null ])
        </div>
        
        <div class="col-md-4 ">       
            @include('sgcontent::inputs.text', ['fieldname' => 'title', 'fieldlabel' => trans('sgcontent::sgcontent.Title'), 'fieldvalue' => null ])         
            @include('sgcontent::inputs.text', ['fieldname' => 'alias', 'fieldlabel' => trans('sgcontent::sgcontent.Alias'), 'fieldvalue' => null ])
            
            @include('sgcontent::inputs.image', ['fieldname' => 'image', 'fieldlabel' => trans('sgcontent::sgcontent.ImageContent'), 'fieldvalue' => null ])
             
        </div> 
        
        <div class="col-md-4 text-warning ">      
            @include('sgcontent::inputs.text', ['fieldname' => 'metatitle', 'fieldlabel' => trans('sgcontent::sgcontent.Metatitle'), 'fieldvalue' => null, 'class' => 'metainfo' ])
            @include('sgcontent::inputs.textarea', ['fieldname' => 'metadescription', 'fieldlabel' => trans('sgcontent::sgcontent.Metadescription'), 'fieldvalue' => null, 'class' => 'metainfo'  ])
            @include('sgcontent::inputs.text', ['fieldname' => 'metakeywords', 'fieldlabel' => trans('sgcontent::sgcontent.Metakeywords'), 'fieldvalue' => null, 'class' => 'metainfo' ])
   
        </div>
        
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            @include('sgcontent::inputs.textarea', ['fieldname' => 'text', 'fieldlabel' => trans('sgcontent::sgcontent.Text'), 'fieldvalue' => null, 'class' => 'cktextarea' ])
        </div> 
    </div>

</div> 


@push('footer_scripts')
{!! Html::script('assets/ckeditor/ckeditor.js') !!}
<script>
    $(function () {
        $('.cktextarea').each(function (index) {
            CKEDITOR.replace($(this).attr('name'),{
                allowedContent: true,
                filebrowserBrowseUrl : '/elfinder/ckeditor' });
        });

    });
</script>
@endpush