@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontent::sgcontent.Edit Items Content')
@stop

@section('page_header')
    @lang('sgcontent::sgcontent.Edit Items Content')
@stop  


@section('tools_panel')
    {!! Form::model($content, array('method' => 'PATCH', 'route' => array('adminsc.sgcontent.contents.update', $content->id), 'files'=>true)) !!}

    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Update'), 'route' => 'adminsc.sgcontent.contents.index', 'continue' => true ])
    @endsection
@endsection



@section('content') 

@include('sgcontent::admin.contents.fields')


    {!! Form::close() !!}

    @include('admin.inputs.errorlist')
 
@stop

@section('footer')
@stop    
