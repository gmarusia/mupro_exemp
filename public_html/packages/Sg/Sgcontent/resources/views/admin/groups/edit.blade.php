@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontent::sgcontent.Edit Groups Content')
@stop

@section('page_header')
    @lang('sgcontent::sgcontent.Edit Groups Content')
@stop  



@section('tools_panel')
    {!! Form::model($group, array('method' => 'PATCH', 'route' => array('adminsc.sgcontent.groups.update', $group->id))) !!}
    @parent
    @section('in_tools_panel')
        @include('admin.inputs.toolsbutton_submitcancel', [ 'submit' => trans('admin.Update'), 'route' => 'adminsc.sgcontent.groups.index', 'continue' => true ])
    @endsection
@endsection


@section('content') 

    @include('sgcontent::admin.groups.fields')

    {!! Form::close() !!}

    @include('admin.inputs.errorlist')

@stop
