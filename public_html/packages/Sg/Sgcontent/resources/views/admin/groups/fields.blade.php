<div class="container">
    <div class="row">
        <div class="col-md-5">

            @include('admin.inputs.onoff', ['fieldname' => 'activated', 'fieldlabel' => trans('sgmenu::sgmenu.Activated'), 'fieldvalue' => isset($group)?$group->activated:1 ])

            @include('sgcontent::inputs.text', ['fieldname' => 'alias', 'fieldlabel' => trans('sgcontent::sgcontent.Alias'), 'fieldvalue' => null ])

            @include('sgcontent::inputs.text', ['fieldname' => 'pfxclass', 'fieldlabel' => trans('sgcontent::sgcontent.PrefixClass'), 'fieldvalue' => null ])

        </div>
        <div class="col-md-6 col-md-offset-1">

            @include('sgcontent::inputs.text', ['fieldname' => 'title', 'fieldlabel' => trans('sgcontent::sgcontent.GroupName'), 'fieldvalue' => null ])

            @include('sgcontent::inputs.textarea', ['fieldname' => 'text', 'fieldlabel' => trans('sgcontent::sgcontent.GroupText'), 'fieldvalue' => null, 'class' => '' ])

            <hr/>

            @include('sgcontent::inputs.textarea', ['fieldname' => 'description', 'fieldlabel' => trans('sgcontent::sgcontent.GroupDescription'), 'fieldvalue' => null, 'class' => '' ])

        </div>
    </div>
</div>