@extends('layouts.admin')

@section('metatitle')
    @lang('sgcontent::sgcontent.ContentManager')
@stop

@section('page_header')
    @lang('sgcontent::sgcontent.ContentManager')
@stop


@section('tools_panel')
    @parent
@section('in_tools_panel')
    @include('sgcontent::admin.contents.groupitem', [ 'clsContents' =>'', 'clsGroups' => 'active' ])
    @include('admin.inputs.toolsbutton_add', [ '_add_link' => route('adminsc.sgcontent.groups.create'), 'btn_label' => trans('sgcontent::sgcontent.Add new Groups') ] )
@endsection
@endsection


@section('content') 


<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>@lang('sgcontent::sgcontent.Id')</th>
            <th>@lang('sgcontent::sgcontent.NameGroup')</th> 
            <th>@lang('sgcontent::sgcontent.Alias')</th> 
            <th>@lang('sgcontent::sgcontent.Decription')</th> 
            <th>@lang('sgcontent::sgcontent.Active')</th> 
            <th> </th>
            <th> </th>
        </tr>
    </thead>
    
    @forelse($groups as $group)
        <tr>
            <td>{!! $group->id !!}</td>
            <td>{!! $group->title !!}</td> 
            <td>{!! $group->alias !!}</td> 
            <td>{!! $group->description !!}</td> 
            <td>
                @include('admin.inputs.small.tbl_icon_onoff', [ 'marker' => $group->activated ])
            </td> 
            <td class="text-right">
                @include('admin.inputs.small.tbl_btn_edit', [ 'link' => route('adminsc.sgcontent.groups.edit',[ 'id' => $group->id]) ])
            </td>
            <td>
                @include('admin.inputs.small.tbl_btn_delete', [ 'link' => route('adminsc.sgcontent.groups.destroy',[ 'id' => $group->id]) ])
            </td>    
        </tr>
    @empty
        <tr>
            <td colspan="6" >@lang('sgcontent::sgcontent.Not groups')</td>
        </tr>
    @endforelse    
     
    <tbody>    
</table>

    

@stop
