<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
       aria-expanded="false">@lang('sgcontent::sgcontent.ContentManager')<span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
        <li class="{!! Request::is('adminsc/sgcontent/groups/*') || Request::path() == 'adminsc/sgcontent/groups'?'active':'' !!}">
            {!! link_to_route('adminsc.sgcontent.groups.index', trans('sgcontent::sgcontent.ContentGroups')) !!}
        </li>
        <li class="{!! Request::is('adminsc/sgcontent/contents/*') || Request::path() == 'adminsc/sgcontent/contents'?'active':'' !!}">
            {!! link_to_route('adminsc.sgcontent.contents.index', trans('sgcontent::sgcontent.ContentItems')) !!}
        </li>
    </ul>
</li>
