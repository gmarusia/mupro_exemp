<div class="{{ $article->pfxclass }}">

    <h1>{!! $article->title !!}</h1>

    {{-- <div class="content-image">
        {!! Html::image($article->image, $article->title  ) !!}
    </div>--}}

    <div class="content-text">
        {!! str_limit( $article->text, 300) !!}
    </div>
    <a href="{{ route('sgmenu.' . $article->alias )  }}" class="content-link">@lang('fp.more')</a>
</div>