<div class="container {{ $blog->pfxclass }}" > 

    <div class="row head-block"> 
        <div class="col-xs-12">
            {!! $blog->text !!}
        </div>
    </div>
 
    <div class="row list-block"> 
        <?php $cc_item = 0 ; $an_delay = 0.2 ;?>    
        @foreach($blog->contents as $item)
        <div class="col-md-3 col-sm-6 {!! $item->pfxclass !!} proc-item"  id="proc_item{{ ++$cc_item }}" style="animation-delay: {{ $an_delay }}s;">
            <?php $an_delay = $an_delay + 0.2 ;  ?>
            
            <h3>{{ $item->title }}</h3>
            <div class='item-text'>
                {!! str_limit(strip_tags($item->text),600) !!}
            </div>             
            <a href="#" class="btn btn-default" role="button">@lang('sgcontent::sgcontent.readmore')</a>        
        </div>
        @endforeach
    </div>
    
</div>
