<div class="container-fluid {{ $blog->pfxclass }}" > 
     
    @if($title) 
    <div class="row head-block"> 
        <div class="col-xs-12">
            {!! $blog->text !!}
        </div>
    </div>
    @endif
    
    <div class="row list-block">  
        <?php 
        $cc_item = 0 ; $an_delay = 0.2 ; 
        ?>  
        @foreach($blog->contents as $item) 
            <div class="col-md-4 col-sm-6 col-lg-3 {!! $item->pfxclass !!}" >
                <div class="proc-item " id="proc_item{{ ++$cc_item }}" style="animation-delay: {{ $an_delay }}s;" >
                    <?php $an_delay = $an_delay + 0.2 ;  ?>

                    <div class="proc-item-img">
                        {!! \Html::image($item->image , $item->title, [ 'class' => 'img-responsive' ] ) !!}
                    </div>
                    <a href="{{ route('blog.article.alias', [ 'blogs' => $blog->alias, 'alias' => $item->alias ] ) }}">

                        <div class="proc-item-text">{{ $item->title }}</div>
                        <div class="proc-item-more">@lang('sgcontent::sgcontent.readmore')</div>

                    </a>
                </div> 
            </div>
        @endforeach
    </div>
    
</div>
