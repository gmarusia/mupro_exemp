<div class="row blog-items">
    @foreach($blog->contents as $content)
        <div class="blog-item-content col-md-4">
            <div class="blog-item-content-image ">
                {!! Html::image($content->image, $content->title ) !!}
            </div>
            <div class="blog-item-content-title">
                <h2>{!! $content->title !!}</h2>
            </div>
            <div class="blog-item-content-text">
                {{ str_limit(strip_tags($content->text), 200) }}

                <div class="blog-item-content-more">
                    <a href="{!! route('sgmenu.' . $content->alias) !!}" >@lang('fp.more') ...</a>
                </div>

            </div>
        </div>
    @endforeach
</div>
