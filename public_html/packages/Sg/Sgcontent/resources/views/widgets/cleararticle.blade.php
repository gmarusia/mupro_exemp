@if($clear)
    {!! strip_tags($article->text) !!}
@else
    {!! $article->text !!}
@endif