<style> 
    .block-articles h1{
        color: #26451b;
        font-size: 30px; 
        border-bottom: 2px solid #f7941d ;
        padding: 10px 0;
    }
    .block-articles .item-articles h2{
        font-size: 18px;
        font-weight: bold;
        text-align: left;
        margin-top: 0px;
    }
    
    .block-articles .item-articles{
        font-size: 12px;
        text-align: justify; 
    }
    .block-articles .item-articles .readmore{
        width: 100%;
        text-align: right;
        margin-bottom: 20px;
    }
    
    .block-articles .item-articles .readmore a{
        font-size: 14px;
        text-decoration: none;
        transition: 0.3s;
    }
    .block-articles .item-articles .readmore a:hover{
        text-decoration: none;
        color: #f7941d;
        transition: 0.3s;
    }
    
    .block-articles .img-thumbnail{
        width: 40%;
        margin-right: 10px;
    }
</style>


<div class="row block-articles"> 

    <h1 class="col-xs-12 text-right">
        {!! $articles[0]['groups']['title'] !!} 
    </h1>


    @foreach($articles as $item)
    <div class="col-xs-12 col-sm-4 item-articles {!! $item['pfxclass'] !!}" >
        <?php 
        preg_match('@src="([^"]+)"@', $item['text'], $match); 
        $src = array_pop($match);  
        ?>
        {!! \Html::image($src, '', [ 'class' => 'img-thumbnail pull-left' ] )  !!} 
        <h2>{!! $item['title'] !!}</h2>
        {!! str_limit( strip_tags($item['text']),700) !!} 
        <div class="readmore">
            <a href="{!! route('article', $item['alias']) !!}">@lang('sgcontent::sgcontent.readmore')</a> 
        </div>
    </div>
    @endforeach

</div>

