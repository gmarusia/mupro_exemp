<div class="{{ $article->pfxclass }}">
    @if($title)
        <h1>{!! $article->title !!}</h1>
    @endif

    <div class="content-text">
        {!! $article->text !!}
    </div>
</div>