<?php

namespace Sg\Sgcontent\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model {

    protected $table = 'sg_sgcontents'; 
    public $_pathImg = '/images/contents/';
    protected $_uplFile ;  
    
    protected $fillable = array(    'pid', 
                                    'sg_sgcontent_group_id',
                                    'alias', 
                                    'pfxclass',
                                    'pretext', 
                                    'title',   
                                    'text',   
                                    'tags',
                                    'activated',  
                                    'image',
                                    'order',  
                                    'metatitle',
                                    'metadescription',
                                    'metakeywords'
                                );
    
    protected $guarded = array('id', 'created_at', 'updated_at' );
      
    public static $rules = array(    'activated'         => 'required|numeric',
                                    'sg_sgcontent_group_id'=> 'required|numeric',
                                    'title'             => 'required',        
                                    'alias'             => 'required|unique:sg_sgcontents'
                                );
 
    public function groups() {
        return $this->belongsTo('Sg\Sgcontent\Models\Groups', 'sg_sgcontent_group_id');
    }

    public function setAliasAttribute($value) {
        $this->attributes['alias'] = str_slug($value) ;
    }
  

    /**
     * Фильтровать по категории
     */
    public function scopeOfCategory($query, $catid) {
        if ($catid) {
            return $query->where('sg_sgcontent_group_id', '=', $catid);
        }
        return $query;
    }

    /**
     * Фильтровать по "включено"
     */
    public function scopeOfActivated($query) {
        return $query->where('activated', '=', TRUE );
    }      
    
    /**
     * Фильтровать по значению из сессии
     */
    public function scopeFilterBySet($query) { 
        if (\Session::has('sgcontent_contents')) { 
            foreach(\Session::get('sgcontent_contents') as $field => $value){ 
                $query->where($field, '=', $value);
                
            }  
        }
        return $query;
    }
    
    /**
     * Количество активных записей
     */
    public function getCountByActivated() {
        return (int)$this->OfActivated()->count();
    }
    
    /**
     * Реврайт метода с аплоад картинки
     */
    public static function create(array $attributes = array()) {
        $_pathImg = '/images/contents/' ;
        if (isset($attributes['image']) && !empty($attributes['image']) ) {
            $file = $attributes['image']; 
            $_fileName = str_random(3) . '_' . strtolower(str_replace(' ', '_', $file->getClientOriginalName())) ; 
            \File::makeDirectory(public_path() . $_pathImg, 0775, true, true); 
            if($file->move(public_path() . $_pathImg, $_fileName)){
                $attributes['image'] = $_pathImg . $_fileName ;
            } else {
                $attributes['image'] = '';
            } 
        }
        return parent::create($attributes);
    }
    
    /**
     * Реврайт метода с аплоад картинки
     */
    public function update(array $attributes = [], array $options = []) {

        if (isset($attributes['image']) && !empty($attributes['image'])) {
            $file = $attributes['image'];
            $_fileName = str_random(3) . '_' . strtolower(str_replace(' ', '_', $file->getClientOriginalName()));
            \File::makeDirectory(public_path() . $this->_pathImg, 0775, true, true);
            if ($file->move(public_path() . $this->_pathImg, $_fileName)) {
                $attributes['image'] = $this->_pathImg . $_fileName;
                \File::delete(public_path() . $this->image);
            } else {
                unset($attributes['image']);
            }
        } else {
            unset($attributes['image']);
        }
        return parent::update($attributes, $options);
    }
    
    /**
     * Все позиции по Id категории и активные
     */
    public static function getByIdCategory($idCat){
        return self::where('sg_sgcontent_group_id','=',$idCat)->whereActivated(0)->orderBy('order')->with('groups')->get()->toArray();
    }
    
    /**
     * Gолучить статью по алиасу
     */
    public static function getArticleByAlias($alias){ 
        return self::ofActivated()
                    ->whereAlias($alias)
                    ->with('groups')
                    ->first();
    }
     
}
