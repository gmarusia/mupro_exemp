<?php

namespace Sg\Sgcontent\Models;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model {

    protected $table = 'sg_sgcontent_groups'; 
    protected $fillable = ['activated', 'alias','title', 'description', 'pid', 'text', 'pfxclass'];   
    protected $guarded = ['id', 'created_at', 'updated_at']; 
    
    public static $rules = [ 'alias'     => 'required|max:255|unique:sg_sgcontent_groups',
                            'activated' => 'required|numeric'
                            ];
    
    public function setAliasAttribute($value) {
        $this->attributes['alias'] = str_slug( $value ) ;
    }

    public function contents() {
        return $this->hasMany('Sg\Sgcontent\Models\Content', 'sg_sgcontent_group_id', 'id');
    }
     
    public function subcategories() {
        return $this->hasMany('Sg\Sgcontent\Models\Groups', 'pid');
    }
    
    /**
     * Фильтровать по "включено"
     */
    public function scopeOfActivated($query) {
        return $query->where('activated', '=', TRUE );
    }  

    /**
     * Массив для input select
     */
    public static function getGroupsListForSelect(){
        return self::OfActivated()->lists('title','id'); 
    }  
    
    /**
     * Получить категорию по алиасу со статьями
     */
    public static function getGroupByAliasWithArticles($alias){
        return self::OfActivated()->whereAlias($alias)->with('contents')->first();
    }
}
