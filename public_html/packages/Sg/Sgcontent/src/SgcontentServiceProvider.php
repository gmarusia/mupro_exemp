<?php

namespace Sg\Sgcontent;

use Illuminate\Support\ServiceProvider;

class SgcontentServiceProvider extends ServiceProvider {

    public function register() {
        $this->mergeConfigFrom(__DIR__ . '/../config/sgcontent.php', 'sgcontent');
        
        $this->app->bind('sgcontent', function () {
            return new Sgcontent; 
        });
//        $this->app->bind('contentsevent', function () {
//            return new Lib\Itemsevent; 
//        });
        
    }

    public function boot() {

        $this->publishes([__DIR__ . '/../config/sgcontent.php' => config_path('sgcontent.php')], 'config');
        $this->publishes([__DIR__ . '/../database/migrations' => $this->app->databasePath() . '/migrations'], 'migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'sgcontent');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang/', 'sgcontent');
        
//        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
//        $loader->alias('Itemsevent', 'Sg\Sgcontent\Facades\Itemsevent');

        require __DIR__ . '/Http/routes.php';
    }
     
}
