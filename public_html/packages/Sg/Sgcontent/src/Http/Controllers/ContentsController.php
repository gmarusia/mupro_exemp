<?php

namespace Sg\Sgcontent\Http\Controllers;

use App\Http\Controllers\Controller;
use Sg\Sgcontent\Models\Groups;
use Sg\Sgcontent\Models\Content;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
//use Sg\Sgcontent\Facades\Itemsevent;

class ContentsController extends Controller {

    protected $contents;

    public function __construct(Content $contents) {
        $this->contents = $contents;
    }

    public function index(Request $request) { 
        // Параметр для фильтрации
        if( $request->has('group_id') ){ 
            \Session::put( 'sgcontent_contents.sg_sgcontent_group_id', $request->input('group_id') ); 
        }
        
        return view('sgcontent::admin.contents.index')
                ->withContents($this->contents->FilterBySet()->get())
                ->withGroups(Groups::getGroupsListForSelect()) 
                ;
    }

    /**
     * Форма создания нового контента
     */
    public function create() {
        
        return view('sgcontent::admin.contents.create')
                ->withGroups(Groups::getGroupsListForSelect()) 
                ->withOrderout($this->contents->getCountByActivated() + 1)
                ;
    }

    /**
     * Запись из формы создания нового контента
     */
    public function store(Request $request) {

        $this->validate($request, Content::$rules);
 
        $this->contents->create($request->all());

        return redirect(route('adminsc.sgcontent.contents.index'))
                        ->with(['message' => trans('admin.Position Create'), 'alert-class' => 'alert-success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id) {
//        //
//    }

    /**
     * Форма редактирования контента
     */
    public function edit($id) {
        return view('sgcontent::admin.contents.edit')        
                ->withGroups(Groups::getGroupsListForSelect()) 
                ->withContent(Content::find($id))  
                ; 
    }

    /**
     * Запись формы редактирования контента
     */
    public function update(Request $request, $id) { 
        $rules =  Content::$rules ; 
        $rules['alias'] = 'required|unique:sg_sgcontents,alias,' . $id ;
        
        $this->validate($request, $rules);

        $contents = $this->contents->find($id);
        $contents->update($request->all());

        return redirect(route('adminsc.sgcontent.contents.index'))
                        ->with(['message' => trans('admin.Position Update'), 'alert-class' => 'alert-success']);
    }

    /**
     * Удаление группы меню
     */
    public function destroy($id) {
        $this->contents->destroy($id);
        return redirect(route('adminsc.sgcontent.contents.index'))
                        ->with(['message' => trans('admin.Position Delete'), 'alert-class' => 'alert-success']);
    }
    
    /**
     * Выдать статью по алиасу
     */
    public function getContentByAlias($alias) { 
        return view('sgcontent::frontpage.articles.article')
                ->withArticle($this->contents->whereAlias($alias)->with('groups')->first()) 
                ;
    }
    
}
