<?php

namespace Sg\Sgcontent\Http\Controllers;

use App\Http\Controllers\Controller; 
use Sg\Sgcontent\Models\Content;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request; 

class Fp_contentsController extends Controller {

    protected $contents;

    public function __construct(Content $contents) {
        $this->contents = $contents;
    }

    public function showArticleByAlias($blogs, $alias ) {
        return view('sgcontent::frontpage.articles.blogarticle')->withArticle(Content::getArticleByAlias($alias));
    }    
}
