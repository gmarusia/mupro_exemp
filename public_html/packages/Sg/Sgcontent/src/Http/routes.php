<?php

//Route::group(['prefix' => 'sgcontent', 'namespace' => 'Sg\Sgcontent\Http\Controllers'], function () {
//
//    Route::get('/', ['as' => 'sgcontent.index', 'uses' => 'SgcontentController@index']);
//});

/* Frontpage ------------------------------------------------------------------- */
Route::group([  'prefix' => '/articles/', 
                'namespace' => 'Sg\Sgcontent\Http\Controllers'
            ], 
                function() {
                            Route::get('{alias}.html',[  'as' => 'article', 
                                                    'uses' => 'ContentsController@getContentByAlias']
                                    );
//                            Route::get('/{blogs}/{alias}.html',[  'as' => 'blog.article.alias',
//                                                    'uses' => 'Fp_contentsController@showArticleByAlias']
//                                    );
                            }
            );
            
//Route::get('/{blogs}/{alias}.html',[  'as' => 'blog.article.alias',
//                        'uses' => 'Sg\Sgcontent\Http\Controllers\Fp_contentsController@showArticleByAlias']
//        );

/* Administrator --------------------------------------------------------------- */
//Route::group([  'prefix' => 'adminsc/sgcontent',
//                'namespace' => 'Sg\Sgcontent\Http\Controllers',
//                'middleware' => ['auth', 'AdminAccess']
//            ],
//                function() {Route::resource('groups', 'GroupController');
//                            Route::resource('contents', 'ContentsController');
//                            }
//            );
