<?php

namespace Sg\Sgcontent\Widgets;

use Illuminate\Contracts\View\View;
use Illuminate\Users\Repository as UserRepository;


class Contentarticles {

    public function compose(View $view) {
        $viewdata = $view->getData();  
        $view->with('articles', \Sg\Sgcontent\Models\Content::getByIdCategory($viewdata['idCat']) );
    }

} 