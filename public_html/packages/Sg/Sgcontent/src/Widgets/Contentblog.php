<?php

namespace Sg\Sgcontent\Widgets;

use Illuminate\Contracts\View\View; 

class Contentblog {

    public function compose(View $view) {
        $viewdata = $view->getData();  
        $view->with([   'blog' => \Cache::remember('sg_getGroupByAliasWithArticles_' . $viewdata['alias'],
                        config('sgcontent.cache.widgetLong'), function() use($viewdata) {
                                                                     return \Sg\Sgcontent\Models\Groups::getGroupByAliasWithArticles($viewdata['alias']) ;
                                                                 }
                                            )
                    ]);
    }

}
