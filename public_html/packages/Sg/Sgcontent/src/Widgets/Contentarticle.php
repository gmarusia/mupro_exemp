<?php

namespace Sg\Sgcontent\Widgets;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;


class Contentarticle{

    public function compose(View $view) {
        $viewdata = $view->getData();
        $view->withArticle( Cache::remember('wg_getarticle_' . $viewdata['id'], config('sgcontent.cache.widgetLong'), function() use($viewdata){
                                                                        return \Sg\Sgcontent\Models\Content::find($viewdata['id']);
                                                                    }
                                                ))
               ->withClear( isset($viewdata['clear'])?$viewdata['clear']:false )
               ->withTitle( isset($viewdata['title'])?$viewdata['title']:true )
        ;
    }

} 